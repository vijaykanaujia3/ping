<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = [
        'page' => 'home',
        'title' => 'Pingga||Home',
        'active' => 'home'
    ];
    return view('index',$data);
});
Route::get('/aboutus', function () {
    $data = [
        'page' => 'aboutus',
        'title' => 'Pingga||About Us',
        'active' => 'aboutus'
    ];
    return view('index',$data);
});
Route::get('/product', function () {
    $data = [
        'page' => 'product',
        'title' => 'Pingga||Product',
        'active' => 'product'
    ];
    return view('index',$data);
});
Route::get('/contactus', function () {
    $data = [
        'page' => 'contactus',
        'title' => 'Pingga||Contact Us',
        'active' => 'contactus'
    ];
    return view('index',$data);
});
