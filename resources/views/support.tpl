{{include file='includes/header.tpl'}}
{{include file='includes/topmenu.tpl'}}

<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="{{$apphost}}/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Support</li>			
        </div>		
    </div>	
</div>

<div class="container">
    <h3>Supports</h3>
    <hr>
    <p style="margin-bottom:30px;">
        Zoel Infotech provides the support, service and confidence dealers need to grow in today’s market. As a value added business partner,
        Zoel Infotech remains committed to providing its customers with the tools they need to win new business.
    </p>

    <div class="row text-center">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >

                <img src="{{$apphost}}/assets/images/installation.png">
                <h4>Installation Support</h4>
                <p>Zoel Infotech is always here to support its customers while they are on site, by guiding them step-by-step on installation, commissioning and troubleshooting of their products.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
                <img src="{{$apphost}}/assets/images/solution.png">	
                <h4>Solution Design</h4>
                <p style="margin-bottom: 32px;">Discover why so many dealers are turning to Zoel Infotech for Solutions Design. We have the technical expertise and customer care your business requires.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
                <img src="{{$apphost}}/assets/images/training.png ">	
                <h4>Training</h4>
                <p>Zoel Infotech continues to focus on leading the industry in high quality 
                    education to help our customers uncover new opportunities and remain competitive in today’s market.</p>
            </div>
        </div>
    </div><br>
    <div class="row text-center" style="margin-bottom:50px;">
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/dvr.png">
                <h4>Legacy Surveillance DVRs Support</h4>
                <p style="margin-bottom: 54px;">A digital video recorder (DVR) is a device that records video in a digital format to
                    a disk drive or other memory medium within a device.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/networking.png">
                <h4>Networking Resources</h4>
                <p style="margin-bottom: 32px;">Network resources refer to forms of data, information and hardware devices that
                    can be accessed by a group of computers through the use of a shared connection.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/securitycamera.png">
                <h4>Security Camera Resources</h4>
                <p>Security cameras come many types and have seemingly endless feature options. With so many different kinds of home security cameras available,
                    deciding which one makes sense for your home can be a daunting task.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <h2>Support</h2>
            <p>With Zoel Infotech you get free, unlimited support 24/7 from our Customer Experience Team. So how can we help you?</p>
            <a href="mailto:support@zoelinfotech.in"><button class="btn btn-primary pull-left"> support@zoelinfotech.in</button></a>
        </div>
        <div class="col-md-5" style="padding:50px;">
            <img src="{{$apphost}}/assets/images/support.png" class="img-responsive">
        </div>
    </div>
</div><br>

{{include file='includes/footer.tpl'}}
