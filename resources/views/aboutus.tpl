{{include file = 'includes/header.tpl'}}
{{include file = "includes/topmenu.tpl"}}


<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="{{$apphost}}/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">
                <span>About Us</span>
            </li>			
        </div>		
    </div>	
</div>

<div class="container">
    <h3>Company at a glance</h3>
    <hr>

    <div class="row">
        <div class="col-md-5 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <img src="{{$apphost}}/assets/images/about1.png" class="img-responsive">
        </div>
        <div class="col-md-7">
            <h3>We Create, Design and Make it Real</h3>
            <p class="text-justify">Security has been a major area of concern for individual families, residential apartments, gated communities, 
                business and scientific establishments. CCTV Surveillance Systems help in keeping a watch on movement of suspicious persons and intruders. In today's world, 
                working couple are using CCTV Surveillance Systems to watch and monitor the work of housekeeping staff, and caretakers employed at home.</p>
            <p class="text-justify">Zoel Infotech has been in the forefront of supplying quality CCTV Surveillance Systems. 
                We have a variety of CCTV Surveillance Systems that offer complete security to your premises with quality performance, robust back-up facilities and prompt servicing support.</p>
        </div>
    </div><br><br>

    <div class="row">
        <div class="col-md-7 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="skill">
                <h2>Our Skills</h2>
                <p class="text-justify">our skills are define below these are the fields in which we are able to do best .</p>

                <div class="progress-wrap">
                    <h4>CCTV Installation</h4>
                    <div class="progress">
                        <div class="progress-bar  color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
                            <span class="bar-width">85%</span>
                        </div>
                    </div>
                </div>

                <div class="progress-wrap">
                    <h4>IP Solution</h4>
                    <div class="progress">
                        <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                            <span class="bar-width">95%</span>
                        </div>
                    </div>
                </div>

                <div class="progress-wrap">
                    <h4>Door Sensor</h4>
                    <div class="progress">
                        <div class="progress-bar color3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="bar-width">80%</span>
                        </div>
                    </div>
                </div>
                <div class="progress-wrap">
                    <h4>Networking</h4>
                    <div class="progress">
                        <div class="progress-bar color3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                            <span class="bar-width">92%</span>
                        </div>
                    </div>
                </div>
            </div>				
        </div>
        <div class="col-md-5">
            <img src="{{$apphost}}/assets/images/skills.png" class="img-responsive">
        </div>
    </div><br><br>

</div>

<!--<div class="our-team">
        <div class="container">
                <h3>Our Team</h3>	
                <div class="text-center">
                        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <img src="images/services/1.jpg" alt="" >
                                <h4>John Doe</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing eil sed deiusmod tempor</p>
                        </div>
                        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                                <img src="images/services/2.jpg" alt="" >
                                <h4>John Doe</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing eil sed deiusmod tempor</p>
                        </div>
                        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
                                <img src="images/services/3.jpg" alt="" >
                                <h4>John Doe</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing eil sed deiusmod tempor</p>
                        </div>
                </div>
        </div>
</div>-->




{{include file = "includes/footer.tpl"}}