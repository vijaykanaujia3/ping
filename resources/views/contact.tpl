{{include file='includes/header.tpl'}}
{{include file='includes/topmenu.tpl'}}
<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="{{$apphost}}/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Contact</li>			
        </div>		
    </div>	
</div>

<section id="contact-page">
    <div class="container">
        <h3>Contacts</h3>
        <hr>
        <div class="row contact-wrap">
            <div class="status alert alert-success" style="display: none"></div>
            <div class="col-md-6 align-right" style="    float: right;">
                <h2>Drop Your Message</h2>
                <p>To put you in touch with the right person sooner, please contact us.</p>
                {{if $request->session()->has('msg')}}
                    {{$request->session()->pull('msg')}}
                {{/if}}
                {{if (count($errors) > 0)}}
                <div class="alert alert-danger">
                    <ul>
                        {{foreach $errors->all() as $error}}
                        <li>{{$error}}</li>
                        {{/foreach}}
                    </ul>
                </div>
                {{/if}}
                <form action="" method="post" role="form" class="contactForm">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    {{*<div class="form-group">*}}
                        {{*<input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />*}}
                        {{*<div class="validation"></div>*}}
                    {{*</div>*}}
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button></div>
                </form>                       
            </div>
            <div class="col-sm-6 col-md-6 box" style="float:left">
                <h3>Our Office</h3><hr/>
                <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp; GF 2, 27/6, Pratap Bhawan<br>&nbsp;&nbsp;&nbsp;&nbsp; Near Krishi Bhawan, Lucknow - 226001</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +91 - 7668 168 300</p>
                <p><i class="fa fa-fax" aria-hidden="true"></i>&nbsp; 0522 - 4951 303</p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; sales@zoelinfotech.in</p>
            </div>
        </div><!--/.row-->
    </div><!--/.container-->

    <style>


        @media (max-width:968px)
        {

            .box{
                width:98%;
                height:auto;
                font-size:18px;
            }


        }
    </style>
</section><!--/#contact-page-->


{{include file='includes/footer.tpl'}}