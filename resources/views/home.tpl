{{include file = 'includes/header.tpl'}}
{{include file = "includes/topmenu.tpl"}}
<style>

    @media (max-width:968px)
    {
        .box{
            width:98%;
            height:auto;
            font-size:18px;
        }
    }
</style>
<div id="carousel-example-generic" class="carousel slide " data-ride="carousel" style="margin-top: 51px;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner mywidth">
        <div class="item active">
            <img src="{{$apphost}}/assets/images/slider1.png" alt="..." class="img-responsive ">
            <div class="carousel-caption">

            </div>
        </div>
        <div class="item">
            <img src="{{$apphost}}/assets/images/slider2.png" alt="..." class="img-responsive">
            <div class="carousel-caption">

            </div>
        </div>
        <div class="item">
            <img src="{{$apphost}}/assets/images/slider3.png" alt="..." class="img-responsive">
            <div class="carousel-caption">

            </div>
        </div>
        <div class="item">
            <img src="{{$apphost}}/assets/images/slider4.png" alt="..." class="img-responsive">
            <div class="carousel-caption">

            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left mybtn" style="color:white;"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right mybtn" style="color:white;"></span>
    </a>
</div>--> <!-- Carousel -->

<div class="container text-center">
    <h1 class="text-center" style="margin-bottom: 45px;">Core Offerings</h1>
    <div class="row">

        <div class="col-md-3">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >

                <img src="{{$apphost}}/assets/images/security1.png">
                <h4>CCTV </h4>
                <p>Zoel Infotech portfolio of products gives you a lot of options when it comes of choosing the perfect CCTV camera like - Hikvision & more.</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
                <img src="{{$apphost}}/assets/images/boimetric.png">	
                <h4>BIOMETRIC</h4>
                <p>We offer Time Watch Biometric time attendance systems latest technology devices, Includes fingerprint reader or card based.</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
                <img src="{{$apphost}}/assets/images/doorsensor.png ">	
                <h4>DOOR SENSOR</h4>
                <p>We offers high quality door sensors which protect your home and family with the added security of door & window sensors.</p>
            </div>
        </div>


        <div class="col-md-3">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/homesolution.png">
                <h4>HOME SOLUTION</h4>
                <p>Zoel Infotech offers widest range of Security & Surveillance solutions & Home Automation, ensuring you the ease of life.</p>
            </div>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{$apphost}}/services" class="btn btn-primary pull-right">& More...</a>
        </div>
    </div><br>
</div>

<div class=" container-fluid">
    <div class="row">
        <div class="col-md-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >
            <img src="{{$apphost}}/assets/images/about.png" class="img-responsive"/>
        </div>

        <div class="col-md-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
            <h4>Who We Are</h4>
            <h2>Company at a glance</h2>
            <p>
                Zoel Infotech is dedicated to helping your business grow, and as the leading  distributor for
                safety & security products we have the expertise and resources to make it happen. 
                Take advantage of all Zoel Infotech has to offer today!
            </p> 

            <a href="{{$apphost}}/about" class="btn btn-primary pull-left">Know More</a>
        </div>
    </div>
</div>

<div class="container-fluid text-center">
    <h2>Recent Work</h2>

    <div class="col-md-1"></div>  
    <div class="col-md-5 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
        <img src="{{$apphost}}/assets/images/work1.png" class="img-responsive" >
        <h3>N - Computing Installation</h3>
        <p>We provide N Computing which is a desktop virtualization company that manufactures hardware and software
            to create virtual desktops which enable multiple users to simultaneously share a single operating system instance.</p>
    </div>

    <div class="col-md-5 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <img src="{{$apphost}}/assets/images/work2.png" class="img-responsive" >
        <h3>Smart Classes Setup</h3>
        <p>We provide Smart Classroom Technology that enhanced classrooms that foster opportunities for teaching and learning by integrating learning technology, 
            such as computers, specialized software, audience response technology, assistive listening devices, networking, and audio/visual capabilities</p>
    </div>
    <div class="col-md-1"></div>  
</div>

<!-- Advertisement Dialog Box Start-->
<div class="modal fade" tabindex="-1" role="dialog" id="advertisement">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="{{$apphost}}/assets/images/offer/offer.jpg" class="img-responsive" style="width: 100%;">
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(function(){
       $('#advertisement').modal('show');
    });
</script>
<!--<section id="partner">
<div class="container">
    <div class="center wow fadeInDown">
        <h2>Our Partners</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
    </div>    

    <div class="partners">
        <ul>
            <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="images/partners/partner1.png"></a></li>
            <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="images/partners/partner2.png"></a></li>
            <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="images/partners/partner3.png"></a></li>
            <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="images/partners/partner4.png"></a></li>
            <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="images/partners/partner5.png"></a></li>
        </ul>
    </div>        
</div><!--/.container-->
{{*</section>*}}<!--/#partner-->

<!--<section id="conatcat-info">
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="pull-left">
                    <i class="fa fa-phone"></i>
                </div>
<!--   <div class="media-body">
       <h2>Have a question or need a custom quote?</h2>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation +0123 456 70 80</p>
   </div>-->

</div>
</div>
</div>
</div><!--/.container-->    
</section><!--/#conatcat-info-->

{{include file = "includes/footer.tpl"}}