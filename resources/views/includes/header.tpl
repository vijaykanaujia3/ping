<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Zoel Infotech</title>

        <!-- Bootstrap -->
        <link href="{{$apphost}}/assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="icon" href="{{$apphost}}/assets/images/favicon.png" >
        <link rel="stylesheet" href="{{$apphost}}/assets/css/animate.min.css">
        <link href="{{$apphost}}/assets/css/prettyPhoto.css" rel="stylesheet">
        <link href="{{$apphost}}/assets/css/style.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{$apphost}}/assets/css/font-awesome.min.css">
        <script src="{{$apphost}}/assets/js/jquery-3.2.1.min.js"></script>
    </head>
    <body>
