
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navigation">
            <div class="container">					
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed myfun" data-toggle="collapse" data-target=".navbar-collapse.collapse" style="margin-right: 20px;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="">
                        <a href="{{$apphost}}/home"><img src="{{$apphost}}/assets/images/zoellogo.png" class="img-responsive"></a>
                    </div>
                </div>

                <div class="navbar-collapse collapse">							
                    <div class="menu">
                        <ul class="nav navbar-nav" >
                            <li class="home" ><a href="{{$apphost}}/home">Home</a></li>
                            <li class=""><a href="{{$apphost}}/about">About Us</a></li>
                            <li class="" ><a href="{{$apphost}}/support">Support</a></li>								
                            <li class="" ><a href="{{$apphost}}/services">Services</a></li>
                            <li class=""><a href="{{$apphost}}/contact">Contact</a></li>						
                        </ul>
                    </div>
                </div>						
            </div>
        </div>	
    </nav>
</div>

<script>
    var url = window.location;
// Will only work if string in href matches with location
    $('ul.nav a[href="' + url + '"]').parent().addClass('active');

// Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');
     if(window.location.pathname == "/zoelinfotech1/")
        {
            $(".home").addClass("active")
        }
</script>


<style>


    @media (max-width:968px)
    {
        .menu{ float:left;

        }
    }

    </style>

    <script>
        //active link
        t = window.location.pathname.match(/\/.*\/(.*)/)[1];
        x = $('#nav li a');
        for (var i = 0; i <= x.length; i++) {
            m = $(x[i]).attr('href').match(/\/.*\/(.*)/)[1];
            //alert(t);
            // alert(m);
            if (t == '' && m == 'home')
                $($('#nav li')[0]).addClass('active');
            if (t === m)
                $($('#nav li')[i]).addClass('active');
        }
    </script>