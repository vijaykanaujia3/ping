<footer>
    <div class="footer">
        <div class="container text-center">
            <div class="row">
                <div class="social-icon">
                    <div class="col-md-12">
                        <ul class="social-network">
                            <li><a href="https://www.facebook.com/zoel.infotech" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>

                        </ul>	
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="copyright">
                        &copy; Zoel infotech. All Rights Reserved.
                    </div>
                </div>	
                <div class="col-md-6 col-md-offset-2">
                    <div class="copyright">
                        Conceptualized, Designed & Maintained with <i class="fa fa-heart" aria-hidden="true"></i> by Techcomp Solutions
                    </div>
                </div>	
            </div> 
            <div class="pull-left">
                <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
            </div>		
        </div>
    </div>
</footer>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{$apphost}}/assets/js/bootstrap.min.js"></script>
<script src="{{$apphost}}/assets/js/jquery.prettyPhoto.js"></script>
<script src="{{$apphost}}/assets/js/jquery.isotope.min.js"></script>  
<script src="{{$apphost}}/assets/js/wow.min.js"></script>
<script src="{{$apphost}}/assets/js/functions.js"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

</body>
</html>