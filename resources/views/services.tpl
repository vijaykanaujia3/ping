{{include file='includes/header.tpl'}}
{{include file='includes/topmenu.tpl'}}

<style>
    .size{
        width: 220px;
        height: 229px
    }
</style>

<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>

    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="{{$apphost}}/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>            
            </li>
            <li class="active">
                <span>Services</span>
            </li>			
        </div>		
    </div>	
</div>

<div class="container">
    <h3>Services</h3>
    <hr>
    <p style="margin-bottom:30px;">
        All our services are backed by the best trained team in the industry. Zoel Infotech provides the support,
        service and confidence dealers need to grow in today’s market. As a value added business partner,
        Zoel Infotech remains committed to providing its customers with the tools they need to win new business.
    </p>
    <div class="row text-center">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >

                <img src="{{$apphost}}/assets/images/security1.png">
                <h4>CCTV </h4>
                <p>Zoel Infotech portfolio of products gives you a lot of options when it comes of choosing the perfect CCTV camera like - Hikvision, CP Plus, Dadua & many more. 
                    It includes megapixel options, covert security cameras, HD varieties and LCD monitors.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
                <img src="{{$apphost}}/assets/images/boimetric.png">	
                <h4>BIOMETRIC</h4>
                <p>We offer Time Watch Biometric time attendance systems latest technology devices, Includes fingerprint reader or card based (Proximity Smart Card) Biometric devices to reduce time spent in non-core activities by employees.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
                <img src="{{$apphost}}/assets/images/doorsensor.png ">	
                <h4>DOOR SENSOR</h4>
                <p>We offers high quality door sensors which protect your home and family with the added security of door & window sensors. 
                    We offers top brands door sensors. We provide door sensors with high efficiency and cost reliability.</p>
            </div>
        </div>
    </div><br>

    <div class="row text-center" style="margin-bottom:50px;">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/homesolution.png">
                <h4>HOME SOLUTION</h4>
                <p>
                    Zoel Infotech offers widest range of Security & Surveillance solutions with the products from ranging from Wi Fi Cameras, Intrusion Alarms, Digital Locks, Video door phone & 
                    Home Automation etc, ensuring you the ease of life.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/ncomputing.png">
                <h4>N - COMPUTING</h4>
                <p>We provide N Computing which is a desktop virtualization company that manufactures hardware and software to create virtual desktops which enable multiple users to simultaneously share a single operating system instance.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="{{$apphost}}/assets/images/monitorsupplier.png">
                <h4>MONITOR SUPPLIER</h4>
                <p>We are large supplier of leading brand LG LED Monitors. We offers LG Monitors on a reasonable price. 
                    Discover the latest innovations in performance and technology with computer products from LG. 
                    LG computer monitors have something for everyone.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <h2>Contact</h2>
            <p>With Zoel Infotech you get free, unlimited support 24/7 from our Customer Experience Team. So how can we help you?</p>
            <a><button class="btn btn-primary pull-left">0522 - 4951 303</button></a>
        </div>
        <div class="col-md-5" style="padding:50px;">
            <img src="{{$apphost}}/assets/images/contact.png" class="img-responsive">
        </div>
    </div>
</div>

{{*<section id="portfolio">	
<div class="container">
<h3>Services</h3>
<hr>
<p style="margin-bottom:30px;">
All our services are backed by the best trained team in the industry.Zoel Infotech provides the support,
service and confidence dealers need to grow in today’s market. As a value added business partner,
Zoel Infotech remains committed to providing its customers with the tools they need to win new business.
</p>


<ul class="portfolio-filter text-center">
<li><a class="btn btn-default active" href="#" data-filter="*">All Works</a></li>
<li><a class="btn btn-default" href="#" data-filter=".bootstrap">CCTV</a></li>
<li><a class="btn btn-default" href="#" data-filter=".html">DOOR SENSOR</a></li>
<li><a class="btn btn-default" href="#" data-filter=".wordpress">BIOMETRIC</a></li>
<li><a class="btn btn-default" href="#" data-filter=".apps">IP SOLUTION</a></li>
</ul><!--/#portfolio-filter-->

</div>
<div class="container">
<div class="">
<div class="portfolio-items">
<div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/ipsolution2.jpg" alt="" >
<div class="overlay">
<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> 
</div>
</div>
</div><!--/.portfolio-item-->

<div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/slider3.jpg" alt="" >
<div class="overlay">
<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>
</div>
</div>          
</div><!--/.portfolio-item-->

<div class="portfolio-item bootstrap  col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/cctv.jpg" alt="">
<div class="overlay">
<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> 
</div>
</div>        
</div><!--/.portfolio-item-->

<div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/cctv21.jpg" alt="">
<div class="overlay">
<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> 
</div>
</div>      
</div><!--/.portfolio-item-->

<div class="portfolio-item  wordpress col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/adhar.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>         
</div><!--/.portfolio-item-->

<div class="portfolio-item wordpress col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive   img-rounded size"  src="{{$apphost}}/assets/images/biometric1.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->

<div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/intrusion2.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size"  src="{{$apphost}}/assets/images/ipsolution.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item apps  col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive img-rounded size"  src="{{$apphost}}/assets/images/ip.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item wordpress  col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive img-rounded size"  src="{{$apphost}}/assets/images/biometricmachine.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item wordpress  col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive   img-rounded size"  src="{{$apphost}}/assets/images/slider9.jpg" alt="">
<div class="overlay">
<!--<div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div>--> 
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/door2.jpg" alt="" >
<div class="overlay">
<!--    <div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> -->
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/door1.jpg" alt="" >
<div class="overlay">
<!--    <div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> -->
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/sensor.jpg" alt="" >
<div class="overlay">
<!--    <div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> -->
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/door-sensors.jpg" alt="" >
<div class="overlay">
<!--    <div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> -->
</div>
</div>          
</div><!--/.portfolio-item-->
<div class="portfolio-item  apps col-xs-12 col-sm-4 col-md-3">
<div class="recent-work-wrap">
<img class="img-responsive  img-rounded size" src="{{$apphost}}/assets/images/ipsolution3.jpg" alt="" >
<div class="overlay">
<!--    <div class="recent-work-inner">
<h3><a href="#">Business theme</a></h3>
<p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

</div> -->
</div>
</div>          
</div><!--/.portfolio-item-->


</div>
</div>
</div>
</section>*}}
<style>
    @media (max-width:968px){

        .size{
            margin-left:53px;
        }
    }
</style>













{{include file='includes/footer.tpl'}}
