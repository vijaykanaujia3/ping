<!-- jquery -->
<script src="{{$apphost}}/assets/js/vendor/jquery-1.12.4.min.js"></script>
<!-- all plugins JS hear -->
<script src="{{$apphost}}/assets/js/popper.min.js"></script>
<script src="{{$apphost}}/assets/js/bootstrap.min.js"></script>
<script src="{{$apphost}}/assets/js/slick.min.js"></script>
<script src="{{$apphost}}/assets/js/owl.carousel.min.js"></script>
<script src="{{$apphost}}/assets/js/jquery.mainmenu.js"></script>
<script src="{{$apphost}}/assets/js/ajax-email.js"></script>
<script src="{{$apphost}}/assets/js/plugins.js"></script>
<!-- main JS -->
<script src="{{$apphost}}/assets/js/main.js"></script>
</body>

<!-- Mirrored from d29u17ylf1ylz9.cloudfront.net/elly-v1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Jun 2018 05:21:46 GMT -->
</html>