<!-- shop-page-wrapper start -->
<div class="container-fluid product-container">
	<div class="row">
		<div class="col-sm-6">
			<div class="product-left-section">
				<h1 class="product-heading">Let's Design</h1>
				<p class="product-sub-heading">Give the shape to your imagination</p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="product-right-section">
			<img class="img-product img-responsive" src="{{$apphost}}/assets/img/product/product.jpg" alt="">
			</div>
		</div>
	</div>
</div>