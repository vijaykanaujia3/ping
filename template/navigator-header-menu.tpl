<!-- menu-arae start -->
<div id="stickymenu" class="main-menu-area solidblockmenu">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 d-sm-none d-xs-none d-md-none d-lg-block">
                <div class="logo">
                    <a href="{{$apphost}}" style="color: #c97178;font-size: 40px;font-family: fantasy;">
                        {{*<img src="{{$apphost}}/assets/img/logo/logo.jpg" alt="">*}}
                        Pinggal
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="main-menu display-none">
                    <nav>
                        <ul>
                            <li class="{{if $active == 'home'}}active{{/if}}"><a href="{{$apphost}}">Home</a></li>
                            <li class="{{if $active == 'aboutus'}}active{{/if}}"><a href="{{$apphost}}/aboutus">About Us</a></li>
                            <li class="{{if $active == 'product'}}active{{/if}}"><a href="{{$apphost}}/product">Product</a></li>
                            <li class="{{if $active == 'contactus'}}active{{/if}}"><a href="{{$apphost}}/contactus">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Mobile Menu Area Start -->
                <div class="mobile-menu-area hidden-md hidden-lg">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul>
                                <li class="active"><a href="{{$apphost}}">Home</a></li>
                                <li><a href="{{$apphost}}/aboutus">About Us</a></li>
                                <li><a href="{{$apphost}}/product">Product</a></li>
                                <li><a href="{{$apphost}}/contactus">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Mobile Menu Area End -->
            </div>
        </div>
    </div>
</div>
<!-- menu-arae end -->