<!-- header-top start -->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8">
                <!-- support-area start -->
                <div class="support-area">
                    <div class="single-support">
                        <div class="support-icon">
                            <i class="fa fa-truck"></i>
                        </div>
                        <div class="support-description">
                            <p>Free delivery guarantee</p>
                        </div>
                    </div>
                    <div class="single-support">
                        <div class="support-icon">
                            <i class="fa fa-lock"></i>
                        </div>
                        <div class="support-description">
                            <p>Secure Shopping</p>
                        </div>
                    </div>
                    <div class="single-support">
                        <div class="support-icon">
                            <i class="fa fa-history"></i>
                        </div>
                        <div class="support-description">
                            <p>14 Day Returns</p>
                        </div>
                    </div>
                    <div class="single-support">
                        <div class="support-icon">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                        <div class="support-description">
                            <p>Track Your Order</p>
                        </div>
                    </div>
                </div>
                <!-- support-area end -->
            </div>
            <div class="col-lg-5 col-md-4">
                <div class="top-right-wrapper">
                    <!-- top-dropdown start -->
                    <div class="top-dropdown">
                        <ul>
                            <li class="drodown-show"><a href="#">USD <i class="fa fa-angle-down"></i></a>
                                <ul class="open-dropdown">
                                    <li><a href="#"> € Euro</a></li>
                                    <li><a href="#"> $ US Dollar</a></li>
                                </ul>
                            </li>
                            <li class="drodown-show"><a href="#">English <i class="fa fa-angle-down"></i></a>
                                <ul class="open-dropdown">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">Français</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- top-dropdown end -->
                    <!-- shopping-cart start -->
                    <div class="shopping-cart">
                        <ul>
                            <li><a href="cart.html"><span class="shpping-cart-text">Your bag: Total $49.50 </span><span class="item-total">2</span></a>
                                <!-- shopping-cart-wrapper start -->
                                <div class="shopping-cart-wrapper open-dropdown">
                                    <ul>
                                        <li>
                                            <div class="shoping-cart-image">
                                                <a href="single-product.html"><img src="img/product/1.jpg" alt=""></a>
                                                <span class="cart-sticker">1x</span>
                                            </div>
                                            <div class="shoping-product-details">
                                                <h3>Autumn on the new middle-aged men's clothes hot V-neck long-sleeved sweater</h3>
                                                <div class="cart-product-price">
                                                    <span>$16.51</span>
                                                </div>
                                                <div class="product-size"><span><strong>Size</strong>: S</span></div>
                                                <div class="product-color">
                                                    <span><strong>Color</strong>: Yellow</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="shoping-cart-image">
                                                <a href="single-product.html"><img src="img/product/2.jpg" alt=""></a>
                                                <span class="cart-sticker">1x</span>
                                            </div>
                                            <div class="shoping-product-details">
                                                <h3>Contracted casual fashion Men sweater free shipping</h3>
                                                <div class="cart-product-price">
                                                    <span>$25.99</span>
                                                </div>
                                                <div class="product-size"><span><strong>Size</strong>: S</span></div>
                                                <div class="product-color">
                                                    <span><strong>Color</strong>: Yellow</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cart-total">
                                                <h5>Subtotal<span class="float-right">$39.79</span></h5>
                                                <h5>Shipping<span class="float-right">$7.00</span></h5>
                                                <h5>Taxes<span class="float-right">$39.79</span></h5>
                                                <h5>Total<span class="float-right">$0.00</span></h5>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="shoping-checkout">
                                        <a href="checkout.html" class="cart-btn">checkout</a>
                                    </div>
                                </div>
                                <!-- shopping-cart-wrapper end -->
                            </li>
                        </ul>
                    </div>
                    <!-- shopping-cart end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header-top end -->