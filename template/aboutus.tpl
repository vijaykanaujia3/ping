<div class="description-section container-fluid" style="background-color:#7fccf7">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="title-outliner">WHO ARE WE</h1>
            <p style="color:#000000;font-weight:600;">We are the new people in town, A new clothing brand ready to change the way you look at the design and
                comfort as an international brand.
                Here we are with the most awaited quality
                ever. </p>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<div class="container-fluid" style="border-bottom: 1px solid #0e3e9b">
    <div class="row">
        <div class="col-md-5" style="background-color: #00305b">
            <div class="description-section2">
                <h1 class="title-outliner">OUR MISSION</h1>
                <p style="color:#ffffff;">We intend to bring revolution in society by dress up people according to their choice, bring emotion,
                    inspire and implement solution for better comfort and luxury.</p>
            </div>
        </div>
        <div class="col-md-7" style="background-color:#40667c;">
            <div class="description-section2">
                <h1 class="title-outliner">OUR VISION</h1>
                <p style="color:#ffffff;padding: 0px 10%;">We desire to bring eye catching design, high quality products with affordability and make people feel special when they wear it on.</p>
            </div>
        </div>
    </div>
</div>