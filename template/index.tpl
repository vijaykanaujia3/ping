{{include file = './header.tpl'}}
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience. Thanks</p>
<![endif]-->
<!-- Add your application content here -->
<div class="wrapper home-1">
    {{include file = './navigator.tpl'}}
    {{if $page}}
		{{include file = "./"|cat: $page|cat: "-slider.tpl"}}
        {{include file="./"|cat: $page|cat: ".tpl"}}
    {{/if}}
    {{include file = './footer-navigator.tpl'}}

</div>
{{include file = './footer.tpl'}}