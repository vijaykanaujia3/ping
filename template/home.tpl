<div class="description-section container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="title-outliner">WHO ARE WE</h1>
            <p>We are the new people in town, A new clothing brand ready to change the way you look at the design and
                comfort as an international brand.
                Here we are with the most awaited quality
                ever. </p>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<div class="container-fluid" style="border-bottom: 1px solid #0e3e9b">
    <div class="row">
        <div class="col-md-5" style="background-color: #00305b">
            <div class="description-section2">
                <h1 class="title-outliner">OUR VISION</h1>
                <p>We intend to bring revolution in society by dress up people according to their choice, bring emotion,
                    inspire and implement solution for better comfort and luxury.</p>
            </div>
        </div>
        <div class="col-md-7" style="padding: 0px;height: 500px;">
            <img class="img-mission img-responsive" src="{{$apphost}}/assets/img/about/mission.jpg" alt="">
        </div>
    </div>
</div>

<div class="description-section container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="title-outliner">OUR MISSION</h1>
            <p>We desire to bring eye catching design, high quality products with affordability and make people feel
                special when they wear it on. </p>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="headquarter" style="padding-top: 90px">
                <h1 class="title-outliner" style="color: #000;">HEADQUARTER</h1>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-bordered contact-us">
                <tbody>
                <tr>
                    <td width="30%">
                        <p class="text-center"><i class="fa fa-mobile"></i></p>
                        <p class="text-center"><span>+91 8543836611</span></p>
                        <p class="text-center"><span>+91 7899989375</span></p>
                    </td>
                    <td width="30%">
                        <p class="text-center"><i class="fa fa-envelope"></i></p>
                        <p class="text-center"><span>purepiggal@gmail.com</span></p>
                    </td>
                    <td width="40%">
                        <p class="text-center"><i class="fa fa-map-marker"></i></p>
                        <p class="text-center">Purepinggal Pvt Ltd.
                            mishrapur, kursi road, Guramba
                            Lucknow - 226 026. India.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3557.1452032068014!2d80.97416431452106!3d26.930610865803573!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399957d0d02b6a79%3A0x4226c5ca77edfa44!2sKursi+Rd%2C+Guramba%2C+Uttar+Pradesh!5e0!3m2!1sen!2sin!4v1542563433045" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="text-center">
                        <div class="social-link">
                            <ul>
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>