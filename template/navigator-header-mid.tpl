<!-- header-mid-area start -->
<div class="header-mid-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <!-- logo start -->
                <div class="logo">
                    <a href="index.html"><img src="{{$apphost}}/assets/img/logo/logo.png" alt=""></a>
                </div>
                <!-- logo end -->
            </div>
            <div class="col-xs-12 col-md-8 col-lg-8">
                <div class="mid-right-wrapper">
                    <div class="user-info-top">
                        <ul>
                            <li><a href="#">Welcome, Guest</a></li>
                            <li><a href="login-register.html">Sign in</a></li>
                            <li><a href="checkout.html" class="checkout">Checkout</a></li>
                        </ul>
                    </div>
                    <div class="search-area">
                        <form action="#">
                            <input type="text" placeholder="Search our catalog">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header-mid-area end -->