<!-- footer-area start -->
<footer class="footer-area">
    <div class="footer-botton">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="payment-img">
                        <img src="img/brand/payment.png" alt="">
                    </div>
                    <div class="copy-right">
                        <p>Rights Reserved by <a href="pingal.com" target="_blank">PUREPIGGAL </a> PVT LTD.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area end -->

<!--==== Modal start ====-->
<div class="modal-wrapper">
    <div class="modal fade" id="productModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="modal-inner-area row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <!--product-thumbnail start-->
                            <div class="product-thumbnail">
                                <div class="tab-content large-thum-image">
                                    <div role="tabpanel" class="tab-pane active" id="Thumb1">
                                        <img src="img/product/larg-1.jpg" alt="">
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Thumb2">
                                        <img src="img/product/larg-2.jpg" alt="">
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Thumb3">
                                        <img src="img/product/larg-3.jpg" alt="">
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Thumb4">
                                        <img src="img/product/larg-4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="modal-product-tab">
                                    <ul class="nav product-small-thum slider-vertical">
                                        <li role="presentation" class="active"><a href="#Thumb1" data-toggle="tab"><img src="img/product/s1.jpg" alt=""></a></li>
                                        <li role="presentation"><a href="#Thumb2"  data-toggle="tab"><img src="img/product/s2.jpg" alt=""></a></li>
                                        <li role="presentation"><a href="#Thumb3"  data-toggle="tab"><img src="img/product/s3.jpg" alt=""></a></li>
                                        <li role="presentation"><a href="#Thumb4"  data-toggle="tab"><img src="img/product/s4.jpg" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--product-thumbnail end-->

                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <!-- product-thumbnail-content start -->
                            <div class="quick-view-content">
                                <div class="product-info">
                                    <h2>Autumn on the new middle-aged men's clothes hot V-neck long-sleeved sweater</h2>
                                    <p>Faded short sleeves t-shirt with high neckline. Soft and stretchy material for a comfortable fit. Accessorize with a straw hat and you're ready for summer!</p>
                                    <div class="modal-size">
                                        <h4>Size</h4>
                                        <select>
                                            <option title="S" value="1">S</option>
                                            <option title="M" value="2">M</option>
                                            <option title="L" value="3">L</option>
                                        </select>
                                    </div>
                                    <div class="modal-color">
                                        <h4>Color</h4>
                                        <div class="color-list">
                                            <ul>
                                                <li><a href="#" class="orange active"></a></li>
                                                <li><a href="#" class="paste"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="modal-price-box">
                                        <div class="old-price">$30.50</div>
                                        <div class="new-price">$25.50 <span class="discount">-$5.00</span></div>
                                    </div>
                                    <div class="quick-add-to-cart">
                                        <form class="modal-cart">
                                            <div class="quantity">
                                                <label>Quantity</label>
                                                <div class="cart-plus-minus">
                                                    <input class="cart-plus-minus-box" type="text" value="0">
                                                </div>
                                            </div>
                                            <button class="add-to-cart" type="submit">Add to cart</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- product-thumbnail-content end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--==== Modal end ====-->