<!doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from d29u17ylf1ylz9.cloudfront.net/elly-v1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Jun 2018 05:20:27 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{$apphost}}/assets/img/logo/favicon.png">
    <!-- all CSS hear -->
    <link rel="stylesheet" href="{{$apphost}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/nice-select.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/slick.min.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/mainmenu.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/style.css">
    <link rel="stylesheet" href="{{$apphost}}/assets/css/responsive.css">
    <script src="{{$apphost}}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
</head>
<body>