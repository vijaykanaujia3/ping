<!-- slider-area start -->
<div class="slider-area">
    <div class="slider-active owl-carousel">
        <div class="slider-wrapper" style="background-image:url({{$apphost}}/assets/img/slider/aboutus.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3>&nbsp;</h3>
                                <h2></h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
    </div>
</div>
<!-- slider-area end -->