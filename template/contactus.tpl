<div class="container-fluid contact-container">
	<div class="row">
		<div class="col-sm-6">
			<div class="contact-left-section">
				<h1>Headquarters</h1>
				<p>Purepinggal Pvt Ltd. mishrapur, kursi road, Guramba Lucknow - 226 026. India.</p>
				<p>Phone:  <b>+918543836611, +917899989375</b></p>
				<p>Email: <b>purepiggal@gmail.com</b></p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="contact-right-section">
				<form method="post" action="">
				  <div class="form-group">
					<input type="email" class="form-control" id="email" name="email" placeholder="Email *">
				  </div>
				  <div class="form-group">
					<input type="text" class="form-control" id="name" placeholder="Name *">
				  </div>
				  <div class="form-group">
					<input type="text" id="subject" class="form-control" name="subject" placeholder="Subject">
				  </div>
				  <div class="form-group">
					<textarea type="text" rows="5" id="message" class="form-control" name="message" placeholder="Message"></textarea>
				  </div>
				  <button type="submit" class="btn btn-primary pull-right">Send</button>
				</form>
			</div>
		</div>
		<div class="col-sm-12 text-center">
			<div class="social-link">
				<ul>
					<li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-sm-12">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3557.1452032068014!2d80.97416431452106!3d26.930610865803573!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399957d0d02b6a79%3A0x4226c5ca77edfa44!2sKursi+Rd%2C+Guramba%2C+Uttar+Pradesh!5e0!3m2!1sen!2sin!4v1542563433045" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>