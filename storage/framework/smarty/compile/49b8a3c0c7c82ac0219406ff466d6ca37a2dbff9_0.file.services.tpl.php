<?php
/* Smarty version 3.1.31, created on 2017-06-13 05:01:11
  from "C:\xampp\htdocs\zoelinfotech1\resources\views\services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_593f7197c135b8_65536772',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49b8a3c0c7c82ac0219406ff466d6ca37a2dbff9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoelinfotech1\\resources\\views\\services.tpl',
      1 => 1496473178,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_593f7197c135b8_65536772 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
    .size{
        width: 220px;
        height: 229px
    }
</style>

<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>

    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>            
            </li>
            <li class="active">
                <span>Services</span>
            </li>			
        </div>		
    </div>	
</div>

<div class="container">
    <h3>Services</h3>
    <hr>
    <p style="margin-bottom:30px;">
        All our services are backed by the best trained team in the industry. Zoel Infotech provides the support,
        service and confidence dealers need to grow in today’s market. As a value added business partner,
        Zoel Infotech remains committed to providing its customers with the tools they need to win new business.
    </p>
    <div class="row text-center">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >

                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/security1.png">
                <h4>CCTV </h4>
                <p>Zoel Infotech portfolio of products gives you a lot of options when it comes of choosing the perfect CCTV camera like - Hikvision, CP Plus, Dadua & many more. 
                    It includes megapixel options, covert security cameras, HD varieties and LCD monitors.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/boimetric.png">	
                <h4>BIOMETRIC</h4>
                <p>We offer Time Watch Biometric time attendance systems latest technology devices, Includes fingerprint reader or card based (Proximity Smart Card) Biometric devices to reduce time spent in non-core activities by employees.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/doorsensor.png ">	
                <h4>DOOR SENSOR</h4>
                <p>We offers high quality door sensors which protect your home and family with the added security of door & window sensors. 
                    We offers top brands door sensors. We provide door sensors with high efficiency and cost reliability.</p>
            </div>
        </div>
    </div><br>

    <div class="row text-center" style="margin-bottom:50px;">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/homesolution.png">
                <h4>HOME SOLUTION</h4>
                <p>
                    Zoel Infotech offers widest range of Security & Surveillance solutions with the products from ranging from Wi Fi Cameras, Intrusion Alarms, Digital Locks, Video door phone & 
                    Home Automation etc, ensuring you the ease of life.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/ncomputing.png">
                <h4>N - COMPUTING</h4>
                <p>We provide N Computing which is a desktop virtualization company that manufactures hardware and software to create virtual desktops which enable multiple users to simultaneously share a single operating system instance.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/monitorsupplier.png">
                <h4>MONITOR SUPPLIER</h4>
                <p>We are large supplier of leading brand LG LED Monitors. We offers LG Monitors on a reasonable price. 
                    Discover the latest innovations in performance and technology with computer products from LG. 
                    LG computer monitors have something for everyone.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <h2>Contact</h2>
            <p>With Zoel Infotech you get free, unlimited support 24/7 from our Customer Experience Team. So how can we help you?</p>
            <a><button class="btn btn-primary pull-left">0522 - 4951 303</button></a>
        </div>
        <div class="col-md-5" style="padding:50px;">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/contact.png" class="img-responsive">
        </div>
    </div>
</div>


<style>
    @media (max-width:968px){

        .size{
            margin-left:53px;
        }
    }
</style>













<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
