<?php
/* Smarty version 3.1.31, created on 2017-06-05 11:48:45
  from "/home/zoelinfotech/public_html/resources/views/contact.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5935451de76a12_26522867',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ed23872c7614dc31de09ae3dc8f3fb8128f760a3' => 
    array (
      0 => '/home/zoelinfotech/public_html/resources/views/contact.tpl',
      1 => 1496496012,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_5935451de76a12_26522867 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Contact</li>			
        </div>		
    </div>	
</div>

<section id="contact-page">
    <div class="container">
        <h3>Contacts</h3>
        <hr>
        <div class="row contact-wrap">
            <div class="status alert alert-success" style="display: none"></div>
            <div class="col-md-6 align-right" style="    float: right;">
                <h2>Drop Your Message</h2>
                <p>To put you in touch with the right person sooner, please contact us.</p>
                <?php if ($_smarty_tpl->tpl_vars['request']->value->session()->has('msg')) {?>
                    <?php echo $_smarty_tpl->tpl_vars['request']->value->session()->pull('msg');?>

                <?php }?>
                <?php if ((count($_smarty_tpl->tpl_vars['errors']->value) > 0)) {?>
                <div class="alert alert-danger">
                    <ul>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value->all(), 'error');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
?>
                        <li><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</li>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </ul>
                </div>
                <?php }?>
                <form action="" method="post" role="form" class="contactForm">
                    <input type="hidden" name="_token" value="<?php echo csrf_token();?>
">
                    
                        
                        
                    
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button></div>
                </form>                       
            </div>
            <div class="col-sm-6 col-md-6 box" style="float:left">
                <h3>Our Office</h3><hr/>
                <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp; GF 2, 27/6, Pratap Bhawan<br>&nbsp;&nbsp;&nbsp;&nbsp; Near Krishi Bhawan, Lucknow - 226001</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; +91 - 7668 168 300</p>
                <p><i class="fa fa-fax" aria-hidden="true"></i>&nbsp; 0522 - 4951 303</p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; sales@zoelinfotech.in</p>
            </div>
        </div><!--/.row-->
    </div><!--/.container-->

    <style>


        @media (max-width:968px)
        {

            .box{
                width:98%;
                height:auto;
                font-size:18px;
            }


        }
    </style>
</section><!--/#contact-page-->


<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
