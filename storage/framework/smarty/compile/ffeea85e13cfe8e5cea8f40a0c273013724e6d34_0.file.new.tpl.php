<?php
/* Smarty version 3.1.31, created on 2017-01-21 05:34:35
  from "C:\xampp\htdocs\website\resources\views\new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5882f2ebcf88a7_10783095',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffeea85e13cfe8e5cea8f40a0c273013724e6d34' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\new.tpl',
      1 => 1484915694,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5882f2ebcf88a7_10783095 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
    <head>
        <title>.::Pert ERP::.</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
        <?php echo '<script'; ?>
 defer src="https://code.getmdl.io/1.3.0/material.min.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <!-- Always shows a header, even in smaller screens. -->
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/navbar_logo.png" alt="logo" class="img-responsive">
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="">HOME</a>
                        <a class="mdl-navigation__link" href="">ABOUT</a>
                        <a class="mdl-navigation__link" href="">PRICING</a>
                        <a class="mdl-navigation__link" href="">DEVELOPERS</a>
                        <a class="mdl-navigation__link" href="">KNOWLEDGE BASE</a>
                        <a class="mdl-navigation__link" href="">SIGN IN</a>
                    </nav>
                </div>
            </header>
            <div class="mdl-layout__drawer">
                <span class="mdl-layout-title">Title</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="">Link</a>
                    <a class="mdl-navigation__link" href="">Link</a>
                    <a class="mdl-navigation__link" href="">Link</a>
                    <a class="mdl-navigation__link" href="">Link</a>
                </nav>
            </div>
            <main class="mdl-layout__content">
                <div class="page-content"><!-- Your content goes here --></div>
            </main>
        </div>
    </body>
</html><?php }
}
