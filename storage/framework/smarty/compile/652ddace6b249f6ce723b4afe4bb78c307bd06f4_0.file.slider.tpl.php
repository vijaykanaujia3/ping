<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:02:11
  from "F:\htdocs\ping\template\slider.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf19b13a98fc0_60351462',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '652ddace6b249f6ce723b4afe4bb78c307bd06f4' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\slider.tpl',
      1 => 1541611258,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf19b13a98fc0_60351462 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- slider-area start -->
<div class="slider-area">
    <div class="slider-active owl-carousel">
        <div class="slider-wrapper" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/slider/1.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3>&nbsp;</h3>
                                <h2>FEEL IT</h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
        <!--slider-wrapper start-->
        <div class="slider-wrapper" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/slider/2.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 style2 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3>UNBELIEVABLE</h3>
                                <h2>DESIGNS</h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
    </div>
</div>
<!-- slider-area end --><?php }
}
