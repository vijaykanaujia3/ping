<?php
/* Smarty version 3.1.31, created on 2017-06-05 12:04:01
  from "/home/zoelinfotech/public_html/resources/views/includes/topmenu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_593548b10bb2a6_00770076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e4f8605d96e04de6328cb83d1f3ce28d0b9e25c' => 
    array (
      0 => '/home/zoelinfotech/public_html/resources/views/includes/topmenu.tpl',
      1 => 1496492984,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593548b10bb2a6_00770076 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navigation">
            <div class="container">					
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed myfun" data-toggle="collapse" data-target=".navbar-collapse.collapse" style="margin-right: 20px;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home"><img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/zoellogo.png" class="img-responsive"></a>
                    </div>
                </div>

                <div class="navbar-collapse collapse">							
                    <div class="menu">
                        <ul class="nav navbar-nav" >
                            <li class="home" ><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a></li>
                            <li class=""><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/about">About Us</a></li>
                            <li class="" ><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/support">Support</a></li>								
                            <li class="" ><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/services">Services</a></li>
                            <li class=""><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/contact">Contact</a></li>						
                        </ul>
                    </div>
                </div>						
            </div>
        </div>	
    </nav>
</div>

<?php echo '<script'; ?>
>
    var url = window.location;
// Will only work if string in href matches with location
    $('ul.nav a[href="' + url + '"]').parent().addClass('active');

// Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');
    if(window.location.pathname == "/zoelinfotech/")
        {
            $(".home").addClass("active")
        }
<?php echo '</script'; ?>
>


<style>


    @media (max-width:968px)
    {
        .menu{ float:left;

        }
		}

    </style>

    <?php echo '<script'; ?>
>
        //active link
        t = window.location.pathname.match(/\/.*\/(.*)/)[1];
        x = $('#nav li a');
        for (var i = 0; i <= x.length; i++) {
            m = $(x[i]).attr('href').match(/\/.*\/(.*)/)[1];
            //alert(t);
            // alert(m);
            if (t == '' && m == 'home')
                $($('#nav li')[0]).addClass('active');
            if (t === m)
                $($('#nav li')[i]).addClass('active');
        }
    <?php echo '</script'; ?>
><?php }
}
