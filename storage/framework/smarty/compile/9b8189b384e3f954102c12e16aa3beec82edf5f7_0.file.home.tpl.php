<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:41:23
  from "F:\htdocs\ping\template\home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf1a4430fa0f9_25331662',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9b8189b384e3f954102c12e16aa3beec82edf5f7' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\home.tpl',
      1 => 1542557898,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf1a4430fa0f9_25331662 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="description-section container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="title-outliner">WHO ARE WE</h1>
            <p>We are the new people in town, A new clothing brand ready to change the way you look at the design and
                comfort as an international brand.
                Here we are with the most awaited quality
                ever. </p>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<div class="container-fluid" style="border-bottom: 1px solid #0e3e9b">
    <div class="row">
        <div class="col-md-5" style="background-color: #00305b">
            <div class="description-section2">
                <h1 class="title-outliner">OUR VISION</h1>
                <p>We intend to bring revolution in society by dress up people according to their choice, bring emotion,
                    inspire and implement solution for better comfort and luxury.</p>
            </div>
        </div>
        <div class="col-md-7" style="padding: 0px;height: 500px;">
            <img class="img-mission img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/about/mission.jpg" alt="">
        </div>
    </div>
</div>

<div class="description-section container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="title-outliner">OUR MISSION</h1>
            <p>We desire to bring eye catching design, high quality products with affordability and make people feel
                special when they wear it on. </p>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="headquarter" style="padding-top: 90px">
                <h1 class="title-outliner" style="color: #000;">HEADQUARTER</h1>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-bordered contact-us">
                <tbody>
                <tr>
                    <td width="30%">
                        <p class="text-center"><i class="fa fa-mobile"></i></p>
                        <p class="text-center"><span>+91 8543836611</span></p>
                        <p class="text-center"><span>+91 7899989375</span></p>
                    </td>
                    <td width="30%">
                        <p class="text-center"><i class="fa fa-envelope"></i></p>
                        <p class="text-center"><span>purepiggal@gmail.com</span></p>
                    </td>
                    <td width="40%">
                        <p class="text-center"><i class="fa fa-map-marker"></i></p>
                        <p class="text-center">Purepinggal Pvt Ltd.
                            mishrapur, kursi road, Guramba
                            Lucknow - 226 026. India.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3557.4523757935744!2d80.96013431436948!3d26.920869683124096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399957980784abb5%3A0x711041af30485dfd!2sGudamba+Thaana+Rd%2C+Sector+H%2C+Jankipuram%2C+Lucknow%2C+Uttar+Pradesh+226021!5e0!3m2!1sen!2sin!4v1541340569161"
                                height="400" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="text-center">
                        <div class="social-link">
                            <ul>
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php }
}
