<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:53:27
  from "F:\htdocs\ping\template\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf1a717247b65_82448703',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd98457dbb065666b7a0591642edfa70c6a0af4e1' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\index.tpl',
      1 => 1542561050,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./header.tpl' => 1,
    'file:./navigator.tpl' => 1,
    'file:./footer-navigator.tpl' => 1,
    'file:./footer.tpl' => 1,
  ),
),false)) {
function content_5bf1a717247b65_82448703 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:./header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience. Thanks</p>
<![endif]-->
<!-- Add your application content here -->
<div class="wrapper home-1">
    <?php $_smarty_tpl->_subTemplateRender('file:./navigator.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php if ($_smarty_tpl->tpl_vars['page']->value) {?>
		<?php $_smarty_tpl->_subTemplateRender((("./").($_smarty_tpl->tpl_vars['page']->value)).("-slider.tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        <?php $_smarty_tpl->_subTemplateRender((("./").($_smarty_tpl->tpl_vars['page']->value)).(".tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <?php }?>
    <?php $_smarty_tpl->_subTemplateRender('file:./footer-navigator.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</div>
<?php $_smarty_tpl->_subTemplateRender('file:./footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
