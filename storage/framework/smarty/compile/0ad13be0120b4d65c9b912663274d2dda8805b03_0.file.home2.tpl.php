<?php
/* Smarty version 3.1.31, created on 2017-04-09 13:14:38
  from "C:\xampp\htdocs\website\resources\views\home2.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58ea33be8925e9_42409454',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0ad13be0120b4d65c9b912663274d2dda8805b03' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\home2.tpl',
      1 => 1491743673,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./includes/header.tpl' => 1,
    'file:./includes/footer.tpl' => 1,
  ),
),false)) {
function content_58ea33be8925e9_42409454 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:./includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<style>

    .hover_img{
        position: relative;
        top: 0px;
        transition: transform .2s ease-out;

    }
    #myTab li:hover .hover_img{
        transform: translateY(-6px);
    }


    .activestatus{
        border-bottom: 2px solid #f4511e;
        transition: opacity .2s ease-out,width .2s ease-out;
    }

    .hide_text{
        display: none;
    }

    ._show{
        display: block !important;
    }

    ._show::after{
        border: 10px solid #f4511e;
    }

    #myTab li:hover .hide_text{
        display: block;
    }
</style>

<!--main heading section start-->

<div style=" background-image: url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/bg.jpg); background-size: cover; background-repeat: no-repeat; position: relative;">
    <div class="jumbotron overlay">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <h1>One App</h1>
                <h1>All of Business</h1>
                <p style="letter-spacing: 1.1px;">IT IS AS POWERFULL AS IT IS EASY TO USE</p>
                <div>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="button">SIGN UP FOR FREE</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--main heading section start-->


<!--feature section start-->

<div class="c-content-title-1">

    <h3 class="c-center c-font-uppercase c-font-bold">MAIN Features</h3>
    <div class="c-line-center"></div>
    <p class="text-center">Our wide range of apps cover features you need to run your business easily<br>
        Plus they adjust to the way your business works.
    </p>

</div>

<style>
    ._outer{
        margin: 0;
        padding: 0;
        width: 100%;
        white-space: nowrap;
        /*        overflow-x: auto;*/
        overflow: hidden
    }

    ._outer2{

    }
    ._element{
        float: none;
        display: inline-block;
        
        white-space: nowrap
    }
    ._button_container{
        position: absolute;
        width: 95%;

    }
    ._prev{
        position: absolute;
        left: 1%;       
        top: 20px; 
        z-index: 5000;
    }
    ._next{
        position: absolute;
        right: 5%;
        top:20px;
        z-index: 5000;
    }
</style>
<?php echo '<script'; ?>
>
    $(document).ready(function () {
        $('._outer2').hover(function () {
            $('._next').removeClass('hide');
            $('._prev').removeClass('hide');
        }, function () {
            $('._next').addClass('hide');
            $('._prev').addClass('hide');
        });
    });
       function clk_next() {
            $('._outer').animate({scrollLeft:'+=100'}, 500);
        }

        function clk_prev() {
            $('._outer').animate({scrollLeft:'-=100'}, 500);
        }


<?php echo '</script'; ?>
>

<div class="container-fluid">

    <div class="_outer2" id="_myid">
        <div class="_button_container">
            <div class="_prev hide" onclick="javascript:clk_prev();">
                <button style="width: 35px;height: 90px"><<</button>
            </div>
            <div class="_next hide" onclick="javascript:clk_next();">
                <button style="width: 35px;height: 90px">>></button>
            </div>
        </div>
        <div class="_outer" >


            <div class="text-center _element" style="width: 600px">
                <h4 class="c-font-bold c-font-16" style="padding-right: 10%;">Finance</h4>
                <ul class="nav nav-tabs mycontainer" id="myTab">
                    <li class="activestatus hover_effect">
                        <a href="#accounting" data-toggle="tab" class="active">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/accounting.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text _show"><span class='toggle_hover'>ACCOUNTING</span></h4>
                        </a>
                    </li>

                    <li>
                        <a href="#inventory" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/pos.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>POS</span></h4>
                        </a>
                    </li>

                    <li>
                        <a href="#invoice" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/invoice.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1  c-font-bold c-font-12 hide_text"><span class='toggle_hover'>INVOICE</span></h4>
                        </a>
                    </li>

                    <li>
                        <a href="#report" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/report.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1  c-font-bold c-font-12 hide_text"><span class='toggle_hover'>REPORT</span></h4>
                        </a>
                    </li>

                    <li>
                        <a href="#payment" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/payment.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1  c-font-bold c-font-12 hide_text"><span class='toggle_hover'>PAYMENT</span></h4>
                        </a>
                    </li>

                </ul>

            </div>

            <div class="_element" style="width: 500px">

                <h4 class="text-center c-font-bold c-font-16">G Suite Collaboration</h4>

                <ul class="nav nav-tabs mycontainer" id="myTab">

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/gmail.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>GMAIL</span></h4>
                        </a>

                    </li>



                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/docs.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>DOCS</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/sheets.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>SHEETS</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/slides.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>SLIDES</span></h4>
                        </a>

                    </li>

                </ul>

            </div>

            <div class="_element" style="width: 500px">

                <h4 class="text-center c-font-bold c-font-16" style="padding-right: 25%;">Communicate</h4>

                <ul class="nav nav-tabs mycontainer" id="myTab">

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/hangout.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>HANGOUTS</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/contact.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>CONTACTS</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/whatsapp.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>WHATSAPP</span></h4>
                        </a>

                    </li>

                </ul>

            </div>


            <div class="_element" style="width: 500px">

                <h4 class="text-center c-font-bold c-font-16 c-font-uppercase" style="padding-right: 0%;">storage</h4>

                <ul class="nav nav-tabs mycontainer" id="myTab">

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/drive.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>DRIVE</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/dropbox.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>DROPBOX</span></h4>
                        </a>

                    </li>

                    <li>

                        <a href="#integration" data-toggle="tab">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/cloud.png" class="img-responsive hover_img">
                            <h4 class="text-center togglehide1 c-font-bold c-font-12 hide_text"><span class='toggle_hover'>CLOUD</span></h4>
                        </a>

                    </li>

                </ul>

            </div>
            <div class="clearfix clear"></div>

        </div>
        <div class="clearfix clear"></div>
    </div>

    <div class="tab-content">

        <div class="tab-pane fade in active" id="accounting">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">business accounting easy</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/accounts.png" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="inventory">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">Manage inventory</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/font.png" class="img-responsive">
                </div>
            </div>

        </div>
        <div class="tab-pane fade" id="invoice">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">Invoicing made simple</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/inv.png" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="report">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">easily create reports</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/font.png" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="payment">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">so many payment gateways</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/pay.png" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="integration">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="c-font-uppercase c-font-bold">easily intergrates</h3>
                    <p>
                        Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/app.png" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

</div>




<!--feature section end-->

<!--integration section start-->

<div class="container-fluid c-content-title-1">
    <h3 class="c-center c-font-uppercase c-font-bold">Easily Integrate</h3>
    <div class="c-line-center"></div>
    <div class="row">
        <div class="col-md-6" id="video">
            <video class="embed-container" autoplay loop>
                <source src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/video/Comp2.webm" type="video/webm" media="all"/>
                Your browser does not support HTML5 video.
            </video>
        </div>
        <div class="col-md-6">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/3party_app.png" usemap="#Map" class="img-responsive remove_attr_usemap" alt="apps">
            <map name="Map" id="Map" class="xs_remove">
                <area alt="" title="Gmail" href="javascript:void(0);" shape="rect" coords="30,22,92,70" />
                <area alt="" title="Contacts" href="javascript:void(0);" shape="rect" coords="138,24,204,72" />
                <area alt="" title="Hangouts" href="javascript:void(0);" shape="rect" coords="246,25,314,69" />
                <area alt="" title="Dropbox" href="javascript:void(0);" shape="rect" coords="364,29,426,70" />
                <area alt="" title="Drive" href="javascript:void(0);" shape="rect" coords="33,113,90,160" />
                <area alt="" title="Docs" href="javascript:void(0);" shape="rect" coords="141,115,201,159" />
                <area alt="" title="Sheets" href="javascript:void(0);" shape="rect" coords="252,119,313,160" />
                <area alt="" title="Slides" href="javascript:void(0);" shape="rect" coords="360,121,422,160" />
                <area alt="" title="Keep" href="javascript:void(0);" shape="rect" coords="31,209,87,255" />
                <area alt="" title="Whatsapp" href="javascript:void(0);" shape="rect" coords="146,205,196,253" />
            </map>
        </div>
    </div>
</div>
<!--integration section end-->

<!--responsive section start-->

<div class="container-fluid c-content-title-1">
    <h3 class="c-center c-font-uppercase c-font-bold">Any Time - Any Place - Any Where</h3>
    <div class="c-line-center"></div>
    <div class="row">
        <div class="col-md-6">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/res.png" alt="responsive" class="img-responsive" />
        </div>
        <div class="col-md-6">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>
</div>

<!--responsive section end-->

<!--sign up section start-->

<div class="container-fluid text-center" style="background-color: #273441; color: white;">
    <div class="row">
        <div class="col-md-12">
            <h3 style="color:white;" class="c-font-36">Start Your Free Account Today</h3>
            <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="button">SIGN UP FOR FREE</a>
            <p>Get Instant Access with Free Sign Up</p>
        </div>
    </div>
</div>

<!--sign up section End-->

<!--script-->

<?php echo '<script'; ?>
>
    $(document).ready(function () {
        var width = $(window).width(), height = $(window).height();
        if ((width <= 320) || (width <= 480)) {
            $(".xs_remove").remove();
            $(".remove_attr_usemap").removeAttr("usemap");
        }
        $("#myTab li").click(
                function () {
                    $("#myTab li h4._show").removeClass('_show');
                    $("#myTab li.activestatus").removeClass('activestatus');
                    $(this).find('h4').addClass('_show');
                    $(this).addClass('activestatus');
                }
        );
    });

<?php echo '</script'; ?>
>
<!--script-->

<?php $_smarty_tpl->_subTemplateRender('file:./includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 
<?php }
}
