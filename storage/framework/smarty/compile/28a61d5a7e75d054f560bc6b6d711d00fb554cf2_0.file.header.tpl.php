<?php
/* Smarty version 3.1.31, created on 2017-07-13 16:01:28
  from "C:\xampp\htdocs\zoelinfotech1\resources\views\includes\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59679958e3f603_34576965',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '28a61d5a7e75d054f560bc6b6d711d00fb554cf2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoelinfotech1\\resources\\views\\includes\\header.tpl',
      1 => 1496473184,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59679958e3f603_34576965 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Zoel Infotech</title>

        <!-- Bootstrap -->
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/favicon.png" >
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/animate.min.css">
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/prettyPhoto.css" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css" rel="stylesheet" />
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['appshost']->value;?>
/assets/css/bootstrap.min.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/font-awesome.min.css">
        <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/jquery-3.2.1.min.js"><?php echo '</script'; ?>
>
    </head>
    <body>
<?php }
}
