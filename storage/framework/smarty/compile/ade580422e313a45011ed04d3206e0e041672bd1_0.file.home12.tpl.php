<?php
/* Smarty version 3.1.31, created on 2017-04-06 13:41:53
  from "C:\xampp\htdocs\website\resources\views\home12.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e645a16d7be3_88511068',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ade580422e313a45011ed04d3206e0e041672bd1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\home12.tpl',
      1 => 1491479030,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./includes/header.tpl' => 1,
    'file:./includes/footer.tpl' => 1,
  ),
),false)) {
function content_58e645a16d7be3_88511068 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:./includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<!--main heading section start-->
<div style=" background-image: url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/bg.jpg); background-size: cover; background-repeat: no-repeat; position: relative;">
    <div class="jumbotron overlay">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <h1>One App</h1>
                <h1>All of Business</h1>
                <p style="letter-spacing: 1.1px;">IT IS AS POWERFULL AS IT IS EASY TO USE</p>
                <div>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="button">SIGN UP FOR FREE</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main heading section start-->

<!--feature section start-->

<div class="c-content-title-1">

    <h3 class="c-center c-font-uppercase c-font-bold">MAIN Features</h3>
    <div class="c-line-center"></div>
    <p class="text-center">Our wide range of apps cover features you need to run your business easily<br>
        Plus they adjust to the way your business works.
    </p>

</div>


<div class="container">
    <div class="row">
        <div class="board-inner">

            <div class="row">

                <div class="col-md-4">

                    <h3 class="text-center c-font-18">FINANCE</h3>

                    <div class="row">

                        <div class="col-md-3">

                            <ul class="nav nav-tabs" id="myTab" style="padding: 2.5% 2.5% 2.5% 2.5%;">

                                <li class="active hover_effect">
                                    <a href="#accounting" data-toggle="tab">
                                        <span class="one">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/accounting.png" class="img-responsive">
                                        </span> 
                                        <h4 class="text-center c-font-12"><span>ACCOUNTING</span></h4>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        <div class="col-md-3">             

                            <ul class="nav nav-tabs" id="myTab" style="padding: 2.5% 2.5% 2.5% 2.5%;">

                                <li>
                                    <a href="#inventory" data-toggle="tab">
                                        <span class="two">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/inventory.png" class="img-responsive">
                                        </span>
                                        <h4 class="text-center c-font-12"><span>INVENTORY</span></h4>
                                    </a>
                                </li>  

                            </ul>

                        </div>

                        <div class="col-md-3">

                            <ul class="nav nav-tabs" id="myTab" style="padding: 2.5% 2.5% 2.5% 2.5%;">

                                <li>
                                    <a href="#invoice" data-toggle="tab">
                                        <span class="three">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/invoice.png" class="img-responsive">
                                        </span>
                                        <h4 class="text-center c-font-12"><span>INVOICE</span></h4>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        <div class="col-md-3">

                            <ul class="nav nav-tabs" id="myTab" style="padding: 2.5% 2.5% 2.5% 2.5%;">

                                <li>
                                    <a href="#report" data-toggle="tab">
                                        <span class="four">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/report.png" class="img-responsive">
                                        </span> 
                                        <h4 class="text-center c-font-12"><span>REPORT</span></h4>
                                    </a>
                                </li>  

                            </ul>

                        </div>



                    </div>

                    <div class="row">

                        <div class="col-lg-4">

                            <div class="col-md-2">

                                <ul class="nav nav-tabs" id="myTab" style="padding: 2.5% 2.5% 2.5% 2.5%;">

                                    <li>
                                        <a href="#payment" data-toggle="tab">
                                            <span class="five">
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/payment.png" class="img-responsive">
                                            </span>
                                            <h4 class="text-center c-font-12"><span>PAYMENT GATEWAY</span></h4>
                                        </a>
                                    </li> 

                                </ul>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<!-- tab content-->

<div class="tab-content">

    <div class="tab-pane fade in active" id="accounting">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">Makes business accounting easy</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/accounts.png" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="inventory">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">Manage inventory</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/font.png" class="img-responsive">
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="invoice">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">Invoicing made simple</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/inv.png" class="img-responsive">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="report">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">easily create reports</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/font.png" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="payment">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">so many payment gateways</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/pay.png" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="integration">
        <div class="row">
            <div class="col-md-6">
                <h3 class="c-font-uppercase c-font-bold">easily intergrates</h3>
                <p>
                    Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/app.png" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>


<!--feature section end-->

<!--integration section start-->

<div class="container-fluid c-content-title-1">
    <h3 class="c-center c-font-uppercase c-font-bold">Easily Integrate</h3>
    <div class="c-line-center"></div>
    <div class="row">
        <div class="col-md-6" id="video">
            <video class="embed-container" autoplay loop>
                <source src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/video/Comp2.webm" type="video/webm" media="all"/>
                Your browser does not support HTML5 video.
            </video>
        </div>
        <div class="col-md-6">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/3party_app.png" usemap="#Map" class="img-responsive remove_attr_usemap" alt="apps">
            <map name="Map" id="Map" class="xs_remove">
                <area alt="" title="Gmail" href="javascript:void(0);" shape="rect" coords="30,22,92,70" />
                <area alt="" title="Contacts" href="javascript:void(0);" shape="rect" coords="138,24,204,72" />
                <area alt="" title="Hangouts" href="javascript:void(0);" shape="rect" coords="246,25,314,69" />
                <area alt="" title="Dropbox" href="javascript:void(0);" shape="rect" coords="364,29,426,70" />
                <area alt="" title="Drive" href="javascript:void(0);" shape="rect" coords="33,113,90,160" />
                <area alt="" title="Docs" href="javascript:void(0);" shape="rect" coords="141,115,201,159" />
                <area alt="" title="Sheets" href="javascript:void(0);" shape="rect" coords="252,119,313,160" />
                <area alt="" title="Slides" href="javascript:void(0);" shape="rect" coords="360,121,422,160" />
                <area alt="" title="Keep" href="javascript:void(0);" shape="rect" coords="31,209,87,255" />
                <area alt="" title="Whatsapp" href="javascript:void(0);" shape="rect" coords="146,205,196,253" />
            </map>
        </div>
    </div>
</div>
<!--integration section end-->

<!--responsive section start-->

<div class="container-fluid c-content-title-1">
    <h3 class="c-center c-font-uppercase c-font-bold">Any Time - Any Place - Any Where</h3>
    <div class="c-line-center"></div>
    <div class="row">
        <div class="col-md-6">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/res.png" alt="responsive" class="img-responsive" />
        </div>
        <div class="col-md-6">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>
</div>

<!--responsive section end-->

<!--sign up section start-->

<div class="container-fluid text-center" style="background-color: #273441; color: white;">
    <div class="row">
        <div class="col-md-12">
            <h3 style="color:white;" class="c-font-36">Start Your Free Account Today</h3>
            <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="button">SIGN UP FOR FREE</a>
            <p>Get Instant Access with Free Sign Up</p>
        </div>
    </div>
</div>

<!--sign up section End-->

<!--script-->
<style>
    .hover_img{
        position: relative;
        top: -5px;
    }
</style>
<?php echo '<script'; ?>
>
    var width = $(window).width(), height = $(window).height();
    if ((width <= 320) || (width <= 480)) {
        $(".xs_remove").remove();
        $(".remove_attr_usemap").removeAttr("usemap");
    }
    $(document).ready(function () {
        $('li.hover_effect').mouseover(function () {
            $('.toggle_hover').removeClass('hide');
            $('.hover_img_toggle').addClass('hover_img');
        });
        $('li.hover_effect').mouseout(function () {
            $('.toggle_hover').addClass('hide');
            $('.hover_img_toggle').removeClass('hover_img');
        });
    });
<?php echo '</script'; ?>
>
<!--script-->

<?php $_smarty_tpl->_subTemplateRender('file:./includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 
<?php }
}
