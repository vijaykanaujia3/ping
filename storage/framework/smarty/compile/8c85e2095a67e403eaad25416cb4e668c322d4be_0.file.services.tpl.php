<?php
/* Smarty version 3.1.31, created on 2017-05-29 04:58:02
  from "C:\xampp\htdocs\zoel\resources\views\services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_592baa5a025cb1_15711753',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c85e2095a67e403eaad25416cb4e668c322d4be' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoel\\resources\\views\\services.tpl',
      1 => 1495892747,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_592baa5a025cb1_15711753 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
    .size{
        width: 220px;
        height: 229px
    }
</style>

<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>

    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>            
            </li>
            <li class="active">
                <span>Services</span>
            </li>			
        </div>		
    </div>	
</div>

<section id="portfolio">	
    <div class="container">
        <div class="center">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt</p>
        </div>

        <ul class="portfolio-filter text-center">
            <li><a class="btn btn-default active" href="#" data-filter="*">All Works</a></li>
            <li><a class="btn btn-default" href="#" data-filter=".bootstrap">CCTV</a></li>
            <li><a class="btn btn-default" href="#" data-filter=".html">DOOR SENSOR</a></li>
            <li><a class="btn btn-default" href="#" data-filter=".wordpress">BIOMETRIC</a></li>
            <li><a class="btn btn-default" href="#" data-filter=".apps">IP SOLUTION</a></li>
        </ul><!--/#portfolio-filter-->

    </div>
    <div class="container">
        <div class="">
            <div class="portfolio-items">
                <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/ipsolution2.jpg" alt="" >
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                               
                            </div>>--> 
                        </div>
                    </div>
                </div><!--/.portfolio-item-->

                <div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/slider3.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->

                <div class="portfolio-item bootstrap  col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/cctv.jpg" alt="">
                        <div class="overlay">
                            <!-- <div class="recent-work-inner">
                                 <h3><a href="#">Business theme</a></h3>
                                 <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                             </div>--> 
                        </div>
                    </div>        
                </div><!--/.portfolio-item-->

                <div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/cctv21.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                               
                            </div>--> 
                        </div>
                    </div>      
                </div><!--/.portfolio-item-->

                <div class="portfolio-item  wordpress col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/adhar.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                               
                            </div>--> 
                        </div>
                    </div>         
                </div><!--/.portfolio-item-->

                <div class="portfolio-item wordpress col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive   img-rounded size"  src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/biometric1.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->

                <div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/intrusion2.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size"  src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/ipsolution.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item apps  col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive img-rounded size"  src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/ip.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item wordpress  col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive img-rounded size"  src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/biometricmachine.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item wordpress  col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive   img-rounded size"  src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/slider9.jpg" alt="">
                        <div class="overlay">
                            <!--<div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>
                                
                            </div>--> 
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/door2.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/door1.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/sensor.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item  html col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/door-sensors.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->
                <div class="portfolio-item  apps col-xs-12 col-sm-4 col-md-3">
                    <div class="recent-work-wrap">
                        <img class="img-responsive  img-rounded size" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/ipsolution3.jpg" alt="" >
                        <div class="overlay">
                            <!--    <div class="recent-work-inner">
                                <h3><a href="#">Business theme</a></h3>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority</p>

                              </div> -->
                        </div>
                    </div>          
                </div><!--/.portfolio-item-->


            </div>
        </div>
    </div>
</section><!--/#portfolio-item-->
<style>
    @media (max-width:968px){

        .size{
            margin-left:53px;
        }
    }
</style>













<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
