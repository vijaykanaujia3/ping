<?php
/* Smarty version 3.1.31, created on 2017-05-29 10:02:04
  from "C:\xampp\htdocs\sonyband\resources\views\contact.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_592bf19c216501_03793304',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3c86d26963466f29f7a204aab209be5dba227f7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sonyband\\resources\\views\\contact.tpl',
      1 => 1495893129,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_592bf19c216501_03793304 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Contact</li>			
        </div>		
    </div>	
</div>

<div class="map">

</div>

<section id="contact-page">
    <div class="container">

        <div class="row contact-wrap">
            <div class="status alert alert-success" style="display: none"></div>
            <div class="col-md-6 align-right" style="    float: right;">
                <h2>Drop Your Message</h2>
                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" class="contactForm">
                    <div class="form-group">

                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button></div>
                </form>                       
            </div>
            <div class="col-sm-6 col-md-6  box" style="float:left">
                <h3>Our Office Address</h3> <hr/>
                <p>1244/44</p>
                <p>naza market</p>
                <p> Lucknow</p>
            </div>
        </div><!--/.row-->
    </div><!--/.container-->

    <style>


        @media (max-width:968px)
        {

            .box{
                width:98%;
                height:auto;
                font-size:18px;
            }


        }
    </style>
</section><!--/#contact-page-->


<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
