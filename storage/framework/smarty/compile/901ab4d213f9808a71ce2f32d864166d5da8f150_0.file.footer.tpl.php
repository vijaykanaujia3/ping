<?php
/* Smarty version 3.1.31, created on 2017-05-29 04:58:23
  from "C:\xampp\htdocs\zoel\resources\views\includes\footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_592baa6fba4279_38563779',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '901ab4d213f9808a71ce2f32d864166d5da8f150' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoel\\resources\\views\\includes\\footer.tpl',
      1 => 1495893999,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_592baa6fba4279_38563779 (Smarty_Internal_Template $_smarty_tpl) {
?>
<footer>
    <div class="footer">
        <div class="container text-center">
            <div class="row">
                <div class="social-icon">
                    <div class="col-md-12">
                        <ul class="social-network">
                            <li><a href="https://www.facebook.com/search/top/?q=zoelinfotech" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/login" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/collections/featured" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="https://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>

                        </ul>	
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="copyright">
                        &copy; Zeol infotech. All Rights Reserved.
                    </div>
                </div>	
                <div class="col-md-6 col-md-offset-2">
                    <div class="copyright">
                        Conceptualized, Designed & Maintained with <i class="fa fa-heart" aria-hidden="true"></i> by Techcomp Solutions
                    </div>
                </div>	
            </div> 
            <div class="pull-left">
                <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
            </div>		
        </div>
    </div>
</footer>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/jquery-2.1.1.min.js"><?php echo '</script'; ?>
>	
<!-- Include all compiled plugins (below), or include individual files as needed -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/jquery.prettyPhoto.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/jquery.isotope.min.js"><?php echo '</script'; ?>
>  
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/wow.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/js/functions.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
<?php echo '</script'; ?>
>

</body>
</html><?php }
}
