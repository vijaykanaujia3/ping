<?php
/* Smarty version 3.1.31, created on 2017-04-06 13:42:04
  from "C:\xampp\htdocs\website\resources\views\signup.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e645acc8fe13_31923444',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '54753fbb1a2a80424ce536bfb9597a2fbb7eed43' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\signup.tpl',
      1 => 1491375296,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58e645acc8fe13_31923444 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pert ERP Sign up</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/favicon.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Serif|Roboto" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&subset=all" rel="stylesheet">
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css">
        <style>
            a:hover {
                text-decoration: underline;
            }
            .container {
                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4" style="margin-top:8px;">
                    <form action="#" method="post" class="signup_form">
                        <div class="logo_div">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/signup_logo.png" alt="PertErp" title="PertERP">
                            </a>
                        </div><hr class="hrstyle">
                        <h2 style="font-size: 28px; text-align: center; margin-bottom: 20px;">Sign up</h2>
                        <h5 class="text-center" style="padding-bottom: 10px;">Welcome! Please enter your details.</h5>
                        <div class="left-inner-addon"><i class="fa fa-user"></i>
                            <input type="text" class="form-control" id="" required="required" placeholder="Your name" autofocus="autofocus" title="Your First and Last Name (will appear in your user id)">
                        </div>
                        <div class="left-inner-addon"><i class="fa fa-envelope"></i>
                            <input type="email" class="form-control" id="email" placeholder="Email id" required="required" title="Your Email Id (your login detail will be emailed here)">
                        </div>
                        <div class="left-inner-addon"><i class="fa fa-briefcase"></i>
                            <input type="text" class="form-control" id="" placeholder="Organization" required="required" title="Your Organization name (Your Organization)">
                        </div>
                        <div class="left-inner-addon"><i class="fa fa-phone"></i>
                            <input type="text" class="form-control" id="" placeholder="Mobile number" required="required" title="Your Contact No (We can Assist You)">
                        </div>
                        <div class="input-group"><span class="input-group-addon">http://</span>
                            <input type="text" class="form-control" name="subdomain" placeholder="My company" required="required" title="Your subdomain ie http://mycompany.perterp.com. Must contain only letters, no spaces or special characters.">
                            <span class="input-group-addon">.perterp.com</span>
                        </div>
                        <p class="subdomain"></p>
                        <?php echo '<script'; ?>
>
                            $('input[name="subdomain"]').on('keyup', function (e) {
                                $('p.subdomain').html('http://' + $(this).val() + '.perterp.com');
                            });
                        <?php echo '</script'; ?>
>
                        <div class="checkbox">
                            <label><input type="checkbox"> I agree with</label> <a href="#" style="color: #337ab7;">term of services</a>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="signup_button">Set up my Account</button>
                        </div>
                    </form>
                    <div class="text-center">
                        <h4>Already have an account? | <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/login" style="color: #f4511e;">Log in</a></h4>
                    </div>
                </div>
                <div class="col-sm-4"></div>
            </div><hr class="hrstyle">
            <div class="text-center container" style="margin-bottom: 50px;">
                <p>&copy; 2017 Pert ERP. All Rights Reserved.</p>
            </div>
        </div>
    </body>
</html><?php }
}
