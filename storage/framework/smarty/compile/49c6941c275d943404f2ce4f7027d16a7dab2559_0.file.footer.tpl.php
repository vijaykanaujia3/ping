<?php
/* Smarty version 3.1.31, created on 2017-05-24 10:15:34
  from "C:\xampp\htdocs\website - Copy\resources\views\includes\footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59255d46164d05_57327974',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49c6941c275d943404f2ce4f7027d16a7dab2559' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website - Copy\\resources\\views\\includes\\footer.tpl',
      1 => 1491631979,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59255d46164d05_57327974 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!--footer section end-->

<div>
    <footer class="site-footer">
        <div class="container">
            <div class="row">

                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">FEATURES</h3>
                    <div>
                        <ul>
                            <li>
                                <a href="javascript:callboardinner('accounting');" >Accounting</a>
                            </li>

                            <li>
                                <a href="javascript:callboardinner('inventory');" >Inventory</a>
                            </li>

                            <li>
                                <a href="javascript:callboardinner('invoice');" >Invioce</a>
                            </li>

                            <li>
                                <a href="javascript:callboardinner('report');" >Reports</a>
                            </li>

                            <li>
                                <a href="javascript:callboardinner('payment');" >Payment Gateway</a>
                            </li>
                            <li>
                                <a href="javascript:callboardinner('integration');"> App Integration</a>
                            </li>
                        </ul>
                    </div>
                </section>
                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">ABOUT</h3>
                    <div>
                        <ul>
                            <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/about">About Us</a>
                            </li>

                            <li>
                                <a href="#">Contact</a>
                            </li>

                            <li>
                                <a href="#">Security</a>
                            </li>

                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>

                            <li>
                                <a href="#">Terms of Service</a>
                            </li>
                            <li>
                                <a href="#">Sitemap</a>
                            </li>
                        </ul>
                    </div>
                </section>
                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">HELP</h3>
                    <div>
                        <ul>
                            <li>
                                <a href="#">Our Forum</a>
                            </li>

                            <li>
                                <a href="#">Knowledge Base</a>
                            </li>

                        </ul>
                    </div>
                </section>
                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">GET IN TOUCH</h3>
                    <div>
                        <ul>
                            <li>
                                <a href="mailto:support@perterp.com" title="Support">support@perterp.com</a>
                            </li>

                            <li>
                                <a>+91 - 0000 000 000</a>
                            </li>

                        </ul>
                    </div>
                </section>
                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">SOCIAL</h3>
                    <div>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/Pert-ERP-505417472980816/" target="_blank" title="Facebook">
                                    <i class="fa fa-facebook" aria-hidden="true">
                                    </i>
                                </a>
                            </li>

                            <li>
                                <a href="https://twitter.com/perterpapp" title="Twitter" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true">
                                    </i>
                                </a>
                            </li>

                            <li>
                                <a href="https://www.linkedin.com/company/perterp" title="Linkedin" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true">
                                    </i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </section>
                <section class="col-lg-2 col-md-2 col-xs-6 col-sm-4">
                    <h3 class="c-font-16 c-font-bold">SUBSCRIBE</h3>
                    <div>
                        <form>
                            <div class="input-group">
                                <input type="email" class="form-control" size="50" placeholder="Email Address" required>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>

            <div>
                <a id="back-to-top" href="#" class="back-to-top-btn back-to-top" role="button"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
            </div>

        </div>
    </footer>
</div>
<!--footer section end-->

<!--footer bottom start-->
<div class="bottom-footer c-font-14"> 
    <div class="container">
        <div class="row" style="border-top: 1px solid #eee;">

            <div class="col-md-6" style="margin-top: 15px;">
                <p>Made with <i class="fa fa-heart" aria-hidden="true"></i> in India</p>
            </div>

            <div class="col-md-6 bottom-footerright" style="margin-top: 15px;">
                <p>&copy; 2017 Pert ERP. All Rights Reserved.</p>
            </div>

        </div>
    </div>
</div>
<!--bottom footer end-->
</body>
<?php echo '<script'; ?>
>

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


    //scroll js
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });

    function callboardinner(name) {
        //alert($('#'+name));
        //debugger;
        $('a[href="#' + name + '"]').tab('show');
         $("#myTab li h4._show").removeClass('_show');
                    $("#myTab li.activestatus").removeClass('activestatus');
                     $('a[href="#' + name + '"]').find('h4').addClass('_show');
                     $('a[href="#' + name + '"]').parent('li').addClass('activestatus');
       // $('a[href="#' + name + '"]').trigger('click');
        window.scroll(0, $('a[href="#' + name + '"]').offset().top - 100);

    }
<?php echo '</script'; ?>
>
</html>



<?php }
}
