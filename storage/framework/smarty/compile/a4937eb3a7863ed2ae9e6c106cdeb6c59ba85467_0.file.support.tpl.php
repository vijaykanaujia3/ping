<?php
/* Smarty version 3.1.31, created on 2017-05-29 10:02:01
  from "C:\xampp\htdocs\sonyband\resources\views\support.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_592bf199d9b082_75281093',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4937eb3a7863ed2ae9e6c106cdeb6c59ba85467' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sonyband\\resources\\views\\support.tpl',
      1 => 1495893077,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_592bf199d9b082_75281093 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Support</li>			
        </div>		
    </div>	
</div>

<div class="services">
    <div class="container">
        <div class="row">
            <h3>Supports</h3>
            <hr>
            <div class="col-md-6">
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/service.jpg" class="img-responsive img-rounded" style="width:440px;height:320px;">
                <p>
                    All our services are backed by the best trained team in the industry.Zoel Infotech provides the support,
                    service and confidence dealers need to grow in today’s market. As a value added business partner,
                    Zoel Infotech remains committed to providing its customers with the tools they need to win new business.</p>
            </div>

            <div class="col-md-6">
                <div class="media">
                    <ul>
                        <li>
                            <div class=" media-left">
                                <i class="glyphicon glyphicon-wrench"></i> 					
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Installation Support</h4>
                                <p>Zoel Infotech is always here to support its customers while they are on site,
                                    by guiding them step-by-step on installation, commissioning and troubleshooting of their products..</p>
                            </div>
                        </li>
                        <li>
                            <div class="media-left">
                                <i class="fa fa-book"></i>						
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Solution Design</h4>
                                <p>Discover why so many dealers are turning to Zoel Infotech for Solutions Design.
                                    We have the technical expertise and customer care your business requires.</p>
                            </div>
                        </li>
                        <li>
                            <div class="media-left">
                                <i class="fa fa-rocket"></i>						
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Trainning</h4>
                                <p>Zoel Infotech continues to focus on leading the industry in high quality 
                                    education to help our customers uncover new opportunities and remain competitive in today’s market.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>	

<div class="sub">
    <div class="container">
        <div class="col-md-6">
            <div class="media">
                <ul>
                    <li>
                        <div class="media-left">
                            <i class="fa fa-pencil"></i>						
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Legacy Surveillance DVRs Support</h4>
                            <p>A digital video recorder (DVR) is a device that records video in a digital format to
                                a disk drive or other memory medium within a device.</p>
                        </div>
                    </li>
                    <li>
                        <div class="media-left">
                            <i class="fa fa-book"></i>						
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Networking Resources</h4>
                            <p>Network resources refer to forms of data, information and hardware devices that
                                can be accessed by a group of computers through the use of a shared connection.</p>
                        </div>
                    </li>
                    <li>
                        <div class="media-left">
                            <i class="fa fa-rocket"></i>						
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Security Camera Resources</h4>
                            <p>Security cameras come many types and have seemingly endless feature options. With so many different kinds of home security cameras available,
                                deciding which one makes sense for your home can be a daunting task.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-6">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/servilance2.jpg" class="img-responsive img-rounded" style="width:440px;height:320px;">
            <p>As security systems continue to increase in sophistication, valuable time and training resources are required to confidently configure and service these complex, integrated solutions.

                This powerful fee-based service program for Zoel Infotech resellers delivers an outstanding way to stimulate your business by leveraging our industry-leading customer services. </p>
        </div>
    </div>
</div>


<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
