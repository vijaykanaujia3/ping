<?php
/* Smarty version 3.1.31, created on 2017-06-03 12:08:42
  from "C:\xampp\htdocs\zoelinfotech\resources\views\support.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5932a6ca57e4d7_07985530',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd28d3595ae5d4d8fa3fd95119e5cd30402eb549' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoelinfotech\\resources\\views\\support.tpl',
      1 => 1496475590,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:includes/header.tpl' => 1,
    'file:includes/topmenu.tpl' => 1,
    'file:includes/footer.tpl' => 1,
  ),
),false)) {
function content_5932a6ca57e4d7_07985530 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:includes/topmenu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="breadcrumb">
    <style>
        .breadcrumb li.active {
            color: rgb(85, 172, 238);
        }
    </style>
    <div class="container">	
        <div class="breadcrumb">							
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">Home</a>
                <span class="divider">&nbsp;|&nbsp;</span>
            </li>
            <li class="active">Support</li>			
        </div>		
    </div>	
</div>

<div class="container">
    <h3>Supports</h3>
    <hr>
    <p style="margin-bottom:30px;">
        Zoel Infotech provides the support, service and confidence dealers need to grow in today’s market. As a value added business partner,
        Zoel Infotech remains committed to providing its customers with the tools they need to win new business.
    </p>

    <div class="row text-center">

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" >

                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/installation.png">
                <h4>Installation Support</h4>
                <p>Zoel Infotech is always here to support its customers while they are on site, by guiding them step-by-step on installation, commissioning and troubleshooting of their products.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/solution.png">	
                <h4>Solution Design</h4>
                <p style="margin-bottom: 32px;">Discover why so many dealers are turning to Zoel Infotech for Solutions Design. We have the technical expertise and customer care your business requires.</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/training.png ">	
                <h4>Training</h4>
                <p>Zoel Infotech continues to focus on leading the industry in high quality 
                    education to help our customers uncover new opportunities and remain competitive in today’s market.</p>
            </div>
        </div>
    </div><br>
    <div class="row text-center" style="margin-bottom:50px;">
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/dvr.png">
                <h4>Legacy Surveillance DVRs Support</h4>
                <p style="margin-bottom: 54px;">A digital video recorder (DVR) is a device that records video in a digital format to
                    a disk drive or other memory medium within a device.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/networking.png">
                <h4>Networking Resources</h4>
                <p style="margin-bottom: 32px;">Network resources refer to forms of data, information and hardware devices that
                    can be accessed by a group of computers through the use of a shared connection.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="services hi-icon-wrap hi-icon-effect wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" >
                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/securitycamera.png">
                <h4>Security Camera Resources</h4>
                <p>Security cameras come many types and have seemingly endless feature options. With so many different kinds of home security cameras available,
                    deciding which one makes sense for your home can be a daunting task.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <h2>Support</h2>
            <p>With Zoel Infotech you get free, unlimited support 24/7 from our Customer Experience Team. So how can we help you?</p>
            <a href="mailto:support@zoelinfotech.in"><button class="btn btn-primary pull-left"> support@zoelinfotech.in</button></a>
        </div>
        <div class="col-md-5" style="padding:50px;">
            <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/support.png" class="img-responsive">
        </div>
    </div>
</div><br>

<?php $_smarty_tpl->_subTemplateRender('file:includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
