<?php
/* Smarty version 3.1.31, created on 2017-04-08 06:19:29
  from "C:\xampp\htdocs\website\resources\views\pricing.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e880f1be8696_42545992',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '36d01b02c0d776b469dd00f233ff5ffddb422f9e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\pricing.tpl',
      1 => 1491309633,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./includes/header.tpl' => 1,
    'file:./includes/footer.tpl' => 1,
  ),
),false)) {
function content_58e880f1be8696_42545992 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:./includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="container-fluid text-center c-content-title-1" style="margin-top: 70px;">

    <h3 class="c-center c-font-uppercase c-font-bold">pricing</h3>
    <div class="c-line-center"></div>
    <h2>Choose a Plan that Suites Your Bussiness</h2>
    <h3>A Solution That Pays For Itself</h3>
    <div>
        <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="button">SIGN UP FOR FREE</a>
        <h4>Free for 1 user with 100 MB webspace</h4>
    </div>
        
    <div class="container-fluid text-center" style="margin-top: 70px;">
        <div class="row">
            <div class="col-sm-3">
                <div class="columns">
                    <ul class="price">
                        <span class="lnr lnr-user"></span>
                        <li class="header"><b>Solo</b><br><sup>$</sup><b>0</b></li>
                        <li>1 User</li>
                        <li>100 MB space</li>
                        <li>Email support</li>
                        <li><a href="#" class="pricing-button">Choose Plan</a></li>
                </div>
            </div>
            <div class="col-sm-3" style="border-right: 1px solid rgba(204, 204, 204, 0.45); border-left: 1px solid rgba(204, 204, 204, 0.45);">
                <div class="columns">
                    <ul class="price">
                        <span class="lnr lnr-home"></span>
                        <li class="header"><b>Start Up</b><br><sup>$</sup><b>100</b><sub> /year</sub></li>
                        <li>5 User</li>
                        <li>5 GB space</li>
                        <li>Functional support*</li>
                        <li><a href="#" class="pricing-button">Choose Plan</a></li>
                        <p style="font-size: 10px; text-align: right;">Limited*</p>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3" style="border-right: 1px solid rgba(204, 204, 204, 0.45);">
                <div class="columns">
                    <ul class="price">
                        <span class="lnr lnr-store"></span>
                        <li class="header"><b>Premium</b><br><sup>$</sup><b>150</b><sub> /year</sub></li>
                        <li>25 User</li>
                        <li>10 GB space</li>
                        <li>Functional support*</li>
                        <li><a href="#" class="pricing-button">Choose Plan</a></li>
                        <p style="font-size: 10px; text-align: right;">Limited*</p>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="columns" style="border-right: none;">
                    <ul class="price">
                        <span class="lnr lnr-apartment"></span>
                        <li class="header"><b>Enterprise</b><br><sup>$</sup><b>200</b><sub> /year</sub></li>
                        <li>50 User</li>
                        <li>20 GB space</li>
                        <li>Functional support</li>
                        <li><a href="#" class="pricing-button">Choose Plan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-center">
        <p>No credit cards. No contracts. Cancel anytime. Discover how much easier running your business is and how much more time you have to focus on your clients.<br>
            Get started in just a few clicks.</p><br>
        <h2>Need Help?</h2><br>
        <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="pricing-button" role="button" title="Start Free Account">Contact Us</a>
    </div>
</div>

<?php $_smarty_tpl->_subTemplateRender('file:./includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
