<?php
/* Smarty version 3.1.31, created on 2017-04-04 13:03:01
  from "C:\xampp\htdocs\website\resources\views\blog.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e39985621829_67568190',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '92e9ca37b0db0da5cd0c4084067658ddf6a2cfcc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\blog.tpl',
      1 => 1490700054,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58e39985621829_67568190 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pert ERP | Create a new account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/favicon.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Serif|Roboto" rel="stylesheet">
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css">
    </head>
    <body>
        <style>
            footer
            {
                background:#fff;
                padding:4% 0;
            }
            footer ul
            {
                margin:0;
                padding:0 0 0 ;
            }
            footer ul li,.taber

            {
                color: #000;
                list-style:none;
                margin-bottom:10px;
                font-size:14px
            }
            @media (max-width:723px)
            {
                .taber
                {
                    border-bottom:1px solid #ccc;
                    padding-bottom:10px;
                    cursor:auto;
                    letter-spacing: 1.2px;
                }
                .taber.collapsed:after {
                    font-family: "Glyphicons Halflings";
                    content: "\e114";
                    float: right;
                    margin-left: 15px;
                }
                .taber:after {
                    font-family: "Glyphicons Halflings";
                    content: "\e080";
                    float: right;
                    margin-left: 15px;
                }
                .taber {
                    border-bottom: 1px solid #ccc;
                    cursor: pointer;    
                    padding-bottom: 10px;
                    padding-left: 15px;
                    padding-right: 15px;
                }
                .panel-collapse{
                    padding-left: 15px;
                    padding-right: 15px;
                }
                footer >.container
                {
                    padding-left: 0;
                    padding-right: 0px;
                }
            }

        </style>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2">

                        <div data-toggle="collapse" class="taber" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <b>FEATURES</b>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <ul>
                                <li><a href="#" title="Accounting">Accounting</a></li>
                                <li><a href="#" title="Inventory">Inventory</a></li>
                                <li><a href="#" title="Invoicing">Invoicing</a></li>
                                <li><a href="#" title="Reports">Reports</a></li>
                                <li><a href="#" title="Payment Gateways">Payment Gateways</a></li>
                                <li><a href="#" title="APP Integrations">APP Integrations</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">

                        <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapseOne">
                            <b>ABOUT</b>
                        </div>

                        <div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <ul>
                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/about" title="About Pert ERP">About Us</a></li>
                                <li><a href="#" title="Contact Us">Contact Us</a></li>
                                <li><a href="#" title="Security">Security</a></li>
                                <li><a href="#" title="Privacy Policy">Privacy Policy</a></li>
                                <li><a href="#" title="Terms of Service">Terms of Service</a></li>
                                <li><a href="#" title="Sitemap">Sitemap</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">

                        <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapseOne">
                            <b>HELP</b>
                        </div>
                        <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <ul>
                                <li><a href="#" title="Our Forum">Our Forum</a></li>
                                <li><a href="#" title="Knowledge Base">Knowledge Base</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">

                        <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapseOne">
                            <b>GET IN TOUCH</b>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                           <ul>
                            <li><a href="mailto:support@perterp.com" title="Support">support@perterp.com</a></li>
                            <li><a title="contact">+91 - 0000 000 000</a></li>
                        </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">

                        <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapseOne">
                            <b>SOCIAL</b>
                        </div>
                        <div id="collapsefive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <ul class="list-inline">
                            <li><a href="https://www.facebook.com/Pert-ERP-505417472980816/" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/perterpapp" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/perterp" title="Linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2">

                        <div  class="taber" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapseOne">
                            <b>SUBSCRIBE</b>
                        </div>
                        <div id="collapsesix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                           <form>
                            <div class="input-group">
                                <input type="email" class="form-control" size="50" placeholder="Email Address" required>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-danger"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php echo '<script'; ?>
>
            $(document).ready(function () {

                $("[data-toggle='collapse']").removeAttr("data-toggle");
                if ($(window).width() < 768)
                {
                    $(".taber").attr("data-toggle", "collapse");
                    $('.collapse').collapse("hide");
                }
            });
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
