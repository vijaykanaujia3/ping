<?php
/* Smarty version 3.1.31, created on 2017-04-19 14:10:48
  from "C:\xampp\htdocs\website\resources\views\includes\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58f76fe8d41661_66211902',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77b7774864d2a6b984bec9927ac45c3b24399154' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\includes\\header.tpl',
      1 => 1491660794,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58f76fe8d41661_66211902 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pert ERP | Simple, Free Business Accounting Software</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/favicon.png">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&subset=all" rel="stylesheet">
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <!--navigation bar start-->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar_container">
                <div class="navbar-header">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home"><img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/pert_logo1.png" id="logoimg" class="img-responsive" alt="logo" style="padding: 12px; position: absolute;"></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right" style="margin-right:5px; margin-top: 6px;">
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">HOME</a></li>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/pricing">PRICING</a></li>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/forum">FORUM</a></li>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/blog">BLOG</a></li>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/login">LOGIN</a></li>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" class="navbar-btn navbar-right" role="button">SIGN UP</a>
                    </ul>
                </div>
            </div> 
        </nav>
        <!--navigation bar end-->

        <?php echo '<script'; ?>
>

            $(document).on('touchend', function (event) {
                var clickover = $(event.target);
                var $navbar = $(".navbar-collapse");
                var _opened = $navbar.hasClass("in");
                if (_opened === true && !clickover.hasClass("navbar-toggle")) {
                    $navbar.collapse('hide');
                }
            });

            $('#topnavbar').affix({
                offset: {
                    top: $('#banner').height()
                }
            });


            //active link
         
        <?php echo '</script'; ?>
><?php }
}
