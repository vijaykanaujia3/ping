<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:43:01
  from "F:\htdocs\ping\template\product-slider.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf1a4a5a20669_56053122',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af0ca85f8fe838aed038ecc28ce3c1da65988c77' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\product-slider.tpl',
      1 => 1542562802,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf1a4a5a20669_56053122 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- slider-area start -->
<div class="slider-area">
    <div class="slider-active owl-carousel">
        <div class="slider-wrapper" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/slider/product1.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3>&nbsp;</h3>
                                <h2></h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
        <!--slider-wrapper start-->
        <div class="slider-wrapper" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/slider/product2.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 style2 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3></h3>
                                <h2></h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
		<!--slider-wrapper start-->
        <div class="slider-wrapper" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/slider/product3.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="slider-text style1 style2 slider-text-animation">
                            <div class="slider-bg-text">
                                <h1></h1>
                            </div>
                            <div class="slider-text-info">
                                <h3></h3>
                                <h2></h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-wrapper end-->
    </div>
</div>
<!-- slider-area end --><?php }
}
