<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:53:27
  from "F:\htdocs\ping\template\navigator-header-menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf1a7177f4eb1_26529906',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6aa426a8fe457716a8614440afcd081f464b969d' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\navigator-header-menu.tpl',
      1 => 1536512954,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf1a7177f4eb1_26529906 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- menu-arae start -->
<div id="stickymenu" class="main-menu-area solidblockmenu">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 d-sm-none d-xs-none d-md-none d-lg-block">
                <div class="logo">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
" style="color: #c97178;font-size: 40px;font-family: fantasy;">
                        
                        Pinggal
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="main-menu display-none">
                    <nav>
                        <ul>
                            <li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 'home') {?>active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
">Home</a></li>
                            <li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 'aboutus') {?>active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/aboutus">About Us</a></li>
                            <li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 'product') {?>active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/product">Product</a></li>
                            <li class="<?php if ($_smarty_tpl->tpl_vars['active']->value == 'contactus') {?>active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/contactus">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Mobile Menu Area Start -->
                <div class="mobile-menu-area hidden-md hidden-lg">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul>
                                <li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
">Home</a></li>
                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/aboutus">About Us</a></li>
                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/product">Product</a></li>
                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/contactus">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Mobile Menu Area End -->
            </div>
        </div>
    </div>
</div>
<!-- menu-arae end --><?php }
}
