<?php
/* Smarty version 3.1.31, created on 2018-11-18 17:43:01
  from "F:\htdocs\ping\template\product.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5bf1a4a5b976c1_66749126',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3c65a0e56b4972b39e27fce912597e06bcb45408' => 
    array (
      0 => 'F:\\htdocs\\ping\\template\\product.tpl',
      1 => 1542554576,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf1a4a5b976c1_66749126 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- shop-page-wrapper start -->
<div class="container-fluid product-container">
	<div class="row">
		<div class="col-sm-6">
			<div class="product-left-section">
				<h1 class="product-heading">Let's Design</h1>
				<p class="product-sub-heading">Give the shape to your imagination</p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="product-right-section">
			<img class="img-product img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/img/product/product.jpg" alt="">
			</div>
		</div>
	</div>
</div><?php }
}
