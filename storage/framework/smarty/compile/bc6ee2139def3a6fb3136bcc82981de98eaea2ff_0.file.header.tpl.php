<?php
/* Smarty version 3.1.31, created on 2017-05-29 04:58:23
  from "C:\xampp\htdocs\zoel\resources\views\includes\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_592baa6fb88cf4_52070026',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bc6ee2139def3a6fb3136bcc82981de98eaea2ff' => 
    array (
      0 => 'C:\\xampp\\htdocs\\zoel\\resources\\views\\includes\\header.tpl',
      1 => 1495872555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_592baa6fb88cf4_52070026 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        
        <title>Zoel Infotech</title>

        <!-- Bootstrap -->
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/zoel.jpg" >
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/animate.css">
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/prettyPhoto.css" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css" rel="stylesheet" />
        <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    </head>
    <body><?php }
}
