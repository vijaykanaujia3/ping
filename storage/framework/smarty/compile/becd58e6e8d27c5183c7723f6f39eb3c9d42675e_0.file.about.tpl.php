<?php
/* Smarty version 3.1.31, created on 2017-04-06 13:41:59
  from "C:\xampp\htdocs\website\resources\views\about.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e645a7e5b118_68024519',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'becd58e6e8d27c5183c7723f6f39eb3c9d42675e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\about.tpl',
      1 => 1491473979,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./includes/header.tpl' => 1,
    'file:./includes/footer.tpl' => 1,
  ),
),false)) {
function content_58e645a7e5b118_68024519 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:./includes/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<!--about section start-->

<div class="container-fluid c-content-title-1" style="margin-top: 50px;">

    <h3 class="c-center c-font-uppercase c-font-bold">about us</h3>
    <div class="c-line-center"></div>

    <div class="row">
        
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-signal logo"></span>
        </div>
        
        <div class="col-sm-8">
            <h4>We are a team of blue sky thinkers and seasoned developers who strongly believe</h4>
            <blockquote style="font-size: 32px; color: black;">"It takes a lot of hard work to make some thing simple"</blockquote>
            <p>With varied experience, Laravel (PHP Framework) development is our core strength. Our clear vision for design and development enables us to draft a clear picture of an idea turning into reality. We strongly belive in transparency, an ingrained value in all our products..</p>
        </div>
        
    </div>

    <div class="container-fluid c-content-title-1">

        <h3 class="c-center c-font-uppercase c-font-bold">why us?</h3>
        <div class="c-line-center"></div>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

    </div>

</div>


<!--footer section end-->
<?php $_smarty_tpl->_subTemplateRender('file:./includes/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 <?php }
}
