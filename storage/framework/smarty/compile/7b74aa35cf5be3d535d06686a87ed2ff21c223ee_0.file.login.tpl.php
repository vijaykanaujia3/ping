<?php
/* Smarty version 3.1.31, created on 2017-04-06 13:42:04
  from "C:\xampp\htdocs\website\resources\views\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e645ac73c856_62770188',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b74aa35cf5be3d535d06686a87ed2ff21c223ee' => 
    array (
      0 => 'C:\\xampp\\htdocs\\website\\resources\\views\\login.tpl',
      1 => 1491376059,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58e645ac73c856_62770188 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pert ERP Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/favicon.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Serif|Roboto" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&subset=all" rel="stylesheet">
        <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-2.2.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/css/style.css">
        <style>
            a:hover {
                text-decoration: underline;
            }
            .container {
                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4" style="">
                    <form action="#" method="post" class="signup_form">
                        <div class="logo_div">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/home">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/assets/images/signup_logo.png" alt="PertErp" title="PertERP">
                            </a>
                        </div><hr class="hrstyle">

                        <h3 class="text-center">Login</h3>
                        <h5 class="text-center" style="padding-bottom: 10px;">Welcome! Please enter your login details.</h5>
                        <div class="left-inner-addon"><i class="fa fa-user"></i>
                            <input type="text" class="form-control" id="" placeholder="Username" required="required" autofocus title="Enter your email">
                        </div>
                        <div class="left-inner-addon"><i class="fa fa-lock"></i>
                            <input type="password" class="form-control" id="pwd" placeholder="Password" required="required" title="Your password">
                        </div>
                        <div class="checkbox" style="text-align: left;">
                            <label><input type="checkbox">Keep me signed in</label>
                        </div>
                        <div>
                            <button type="submit" class="signup_button">Log in</button>
                        </div>
                        <div style="margin-top: 10px;" class="text-center">
                            <a href="#" style="color: #f4511e;">Forgot Password?</a>
                        </div>
                    </form>

                    <div class="text-center">
                        <h4>New to Pert ERP? | <a href="<?php echo $_smarty_tpl->tpl_vars['apphost']->value;?>
/signup" style="color: #f4511e;">Sign up</a></h4>
                    </div>
                </div>

                <div class="col-sm-4"></div>
            </div><hr class="hrstyle">
            <div class="text-center container">
                <p>&copy; 2017 Pert ERP. All Rights Reserved.</p>
            </div>
        </div>
    </body>
</html><?php }
}
