<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Swift_Message;

class userController extends Controller
{
    public function sendMail(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|email',
            'subject' => 'required|max:120',
            'message' => 'required',
        ]);
        $userip = $req->ip();
        $getbrowser = $this->getBrowser();
        $msg = '<html><body><p><b>IP Address : </b>'.$userip.'</p>';
        $msg .= '<p><b>System Info : </b>'.$getbrowser.'</p>';
        $msg .= '<p><b>User Email Id : </b>'.$req->email.'</p>';
        $msg .= '<p>'.$req->message.'</p></body></html>';
    try{
        Mail::raw('hello', function ($message) use($req,$msg) {
            /** @var Swift_Message $message */
            $message->addTo('sales@zoelinfotech.in');
            $message->setContentType('text/html');
            $message->setBody($msg);
            $message->setSubject($req->subject);
            $message->setBcc('zoel.infotech@gmail.com');
            $message->setCc('support@zoelinfotech.in');
        });
    }
    catch(Exception $error){
        return redirect()->back()->with('msg', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Message Not send Successfully</div>');
    }
        return redirect()->back()->with('msg', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank You! Message Send Successfully</div>');
    }

    public function getBrowser()
    {
        $browser = "";
        $p = Input::header('user-agent');
        if (preg_match('/Safari/', $p)) {
            $b = ' Safari';
            if (preg_match('/Chrome/', $p)) {
                $b = ' Google Chrome';
            }
            $browser .= $b;
        }
        if (preg_match('/Firefox/', $p)) {
            $browser .= ' Firefox Mozilla';
        }
        if (preg_match('/Windows/', $p)) {
            $browser .= ", Windows Operating System";
        }
        if (preg_match('/Linux/', $p)) {
            $b = ", Linux Operating System";
            if (preg_match('/Linux/', $p)) {
                $b = ", Android Operating System";
            }
            $browser .= $b;
        }
        return empty($browser) ? 'UNKNOWN' : $browser;
    }
}
