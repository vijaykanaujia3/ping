<?php
/**
 * Project:     PertERP: the accounting Software
 * @link      http://www.perterp.com/
 * @author    Aashutosh Sharma <aashu.engineer@gmail.com>
 * @class NotificationMail
 * @package App\Mail
 * @file   NotificationMail.php
 * @version   1.1.0
 * Date: 02-Jun-17
 * Time: 3:39 PM
 * @package App\Mail
 */


namespace App\Mail;


use Illuminate\Mail\Mailable;

class NotificationMail extends Mailable
{
    protected $data = [];

    /**
     * NotificationMail constructor.
     * @param array $data
     */
    public function __construct(array $data = NULL)
    {
        $this->data = $data;
//        $this->textView = $data['message'];
    }

    public function build(){
        return $this->data['message'];
    }

}